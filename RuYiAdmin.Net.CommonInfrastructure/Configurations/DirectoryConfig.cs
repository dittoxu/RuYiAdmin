﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// 目录配置
    /// </summary>
    public class DirectoryConfig
    {
        /// <summary>
        /// 模板目录
        /// </summary>
        public string TemplateDirectory { get; set; }

        /// <summary>
        /// 存储类型
        /// </summary>
        public string StorageType { get; set; }

        /// <summary>
        /// 存储路径
        /// </summary>
        public string StoragePath { get; set; }

        /// <summary>
        /// 临时目录
        /// </summary>
        public string TempPath { get; set; }

        /// <summary>
        /// 审计日志目录
        /// </summary>
        public string MonitoringLogsPath { get; set; }

        /// <summary>
        /// 业务附件目录
        /// </summary>
        public string BusinessAttachmentPath { get; set; }

        /// <summary>
        /// 获取模板路径
        /// </summary>
        /// <returns>模板路径</returns>
        public string GetTemplateDirectory()
        {
            return AppDomain.CurrentDomain.BaseDirectory + "/" + TemplateDirectory;
        }

        /// <summary>
        /// 获取存储路径
        /// </summary>
        /// <returns>存储路径</returns>
        public string GetStoragePath()
        {
            if (StorageType.Equals("Relative"))
            {
                return AppDomain.CurrentDomain.BaseDirectory + "/" + StoragePath;
            }
            else if (StorageType.Equals("Absolute"))
            {
                return StoragePath;
            }

            return string.Empty;
        }

        /// <summary>
        /// 获取临时目录
        /// </summary>
        /// <returns>临时目录</returns>
        public string GetTempPath()
        {
            var path = GetStoragePath() + "/" + TempPath;
            RuYiFileContext.CreateDirectory(path);
            return path;
        }

        /// <summary>
        /// 清空临时目录
        /// </summary>
        public void CleanTempPath()
        {
            RuYiFileContext.ClearDirectory(GetTempPath());
        }

        /// <summary>
        /// 获取审计日志所在目录
        /// </summary>
        /// <returns>审计日志所在目录</returns>
        public string GetMonitoringLogsPath()
        {
            var path = GetStoragePath() + "/" + MonitoringLogsPath;
            RuYiFileContext.CreateDirectory(path);
            return path;
        }

        /// <summary>
        /// 获取业务附件目录
        /// </summary>
        /// <returns>业务附件目录</returns>
        public string GetBusinessAttachmentPath()
        {
            var path = GetStoragePath() + "/" + BusinessAttachmentPath;
            RuYiFileContext.CreateDirectory(path);
            return path;
        }
    }
}
