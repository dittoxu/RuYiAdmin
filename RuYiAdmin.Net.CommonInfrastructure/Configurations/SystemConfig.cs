﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// 系统配置
    /// </summary>
    public class SystemConfig
    {
        /// <summary>
        /// Pem格式Rsa公钥
        /// </summary>
        public string RsaPublicKey { get; set; }

        /// <summary>
        /// Pem格式Rsa私钥
        /// </summary>
        public string RsaPrivateKey { get; set; }

        /// <summary>
        /// 白名单
        /// </summary>
        public string WhiteList { get; set; }

        /// <summary>
        /// Header配置
        /// </summary>
        public string HeaderConfig { get; set; }

        /// <summary>
        /// 机构根目录编号
        /// </summary>
        public Guid OrgRoot { get; set; }

        /// <summary>
        /// 默认密码
        /// </summary>
        public string DefaultPassword { get; set; }

        /// <summary>
        /// AesKey
        /// </summary>
        public string AesKey { get; set; }

        /// <summary>
        /// 登录上限
        /// </summary>
        public int LogonCountLimit { get; set; }

        /// <summary>
        /// TokenKey
        /// </summary>
        public string TokenKey { get; set; }

        /// <summary>
        /// 检测Token开关
        /// </summary>
        public bool CheckToken { get; set; }

        /// <summary>
        /// token有效时间（分钟）
        /// </summary>
        public int UserTokenExpiration { get; set; }

        /// <summary>
        /// 检测JwtToken开关
        /// </summary>
        public bool CheckJwtToken { get; set; }

        /// <summary>
        /// 生产环境是否支持SwaggerUI
        /// </summary>
        public bool SupportSwaggerOnProduction { get; set; }
    }
}
