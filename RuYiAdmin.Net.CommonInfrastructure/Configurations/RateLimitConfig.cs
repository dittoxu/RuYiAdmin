﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// AspNetCoreRateLimit配置
    /// </summary>
    public class RateLimitConfig
    {
        /// <summary>
        /// 实例名称
        /// </summary>
        public string InstanceName { get; set; }
    }
}
