﻿namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// 代码生成器配置
    /// </summary>
    public class CodeGeneratorConfig
    {
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// 忽略字段
        /// </summary>
        public string FieldsIgnoreCase { get; set; }

        /// <summary>
        /// 模型层命名空间
        /// </summary>
        public string EntityNamespace { get; set; }

        /// <summary>
        /// DTO模型命名空间
        /// </summary>
        public string DTONamespace { get; set; }

        /// <summary>
        /// 控制层命名空间
        /// </summary>
        public string ControllerNamespace { get; set; }

        /// <summary>
        /// 服务层命名空间
        /// </summary>
        public string ServiceNamespace { get; set; }

        /// <summary>
        /// 仓储层命名空间
        /// </summary>
        public string RepositoryNamespace { get; set; }
    }
}
