﻿namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// SM4配置
    /// </summary>
    public class SM4Config
    {
        /// <summary>
        /// 模式
        /// CBC或者ECB
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// 密钥
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// 偏移量
        /// </summary>
        public string IV { get; set; }
    }
}
