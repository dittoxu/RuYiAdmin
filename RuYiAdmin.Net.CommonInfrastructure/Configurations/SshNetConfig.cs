﻿using RuYiAdmin.Net.CommonInfrastructure.Classes;

namespace RuYiAdmin.Net.CommonInfrastructure.Configurations
{
    /// <summary>
    /// SshNet配置
    /// </summary>
    public class SshNetConfig
    {
        /// <summary>
        /// Sftp配置
        /// </summary>
        public HostBasicConfig SftpConfig { get; set; }

        /// <summary>
        /// Sftp配置
        /// </summary>
        public HostBasicConfig SshConfig { get; set; }

        /// <summary>
        /// Scp配置
        /// </summary>
        public HostBasicConfig ScpConfig { get; set; }
    }
}
