﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Attributes.Framework.ExcelAttribute
{
    /// <summary>
    /// 数据列表导出标签
    /// </summary>
    public class ExcelExportAttribute : Attribute
    {
        /// <summary>
        /// 列名
        /// </summary>
        public string FieldName { get; }

        /// <summary>
        /// 类型枚举
        /// </summary>
        public string TextEnum { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="fieldName">列名</param>
        /// <param name="textEnum">类型枚举,键值以英文冒号隔开，组之间用英文逗号隔开，如0:男,1:女,2:第三性别</param>
        public ExcelExportAttribute(string fieldName, string textEnum = null)
        {
            FieldName = fieldName;
            if (!string.IsNullOrEmpty(textEnum))
            {
                TextEnum = textEnum;
            }
        }
    }
}
