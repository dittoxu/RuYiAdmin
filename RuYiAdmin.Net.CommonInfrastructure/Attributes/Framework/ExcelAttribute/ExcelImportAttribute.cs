﻿using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Attributes.Framework.ExcelAttribute
{
    /// <summary>
    /// 数据列表导入标签
    /// </summary>
    public class ExcelImportAttribute : Attribute
    {
        /// <summary>
        /// 列名
        /// </summary>
        public string FieldName { get; }

        /// <summary>
        /// 类型枚举
        /// </summary>
        public string TextEnum { get; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="fieldName">列名</param>
        /// <param name="textEnum">类型枚举,键值以英文冒号隔开，组之间用英文逗号隔开，如男:0,女:1,第三性别:2</param>
        public ExcelImportAttribute(string fieldName, string textEnum = null)
        {
            FieldName = fieldName;
            if (!string.IsNullOrEmpty(textEnum))
            {
                TextEnum = textEnum;
            }
        }
    }
}
