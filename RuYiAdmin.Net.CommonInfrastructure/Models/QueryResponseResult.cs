﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace RuYiAdmin.Net.CommonInfrastructure.Models
{
    /// <summary>
    /// 查询结果
    /// </summary>
    public class QueryResponseResult<T> where T : class
    {
        /// <summary>
        /// HttpStatusCode
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }

        /// <summary>
        /// 查询消息
        /// </summary>
        public String Message { get; set; }

        /// <summary>
        /// 查询总记录数
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 查询记录
        /// </summary>
        public List<T> List { get; set; }

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public QueryResponseResult()
        {

        }

        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="httpStatusCode">Http状态码</param>
        /// <param name="message">消息内容</param>
        /// <param name="totalCount">总条数</param>
        /// <param name="list">记录列表</param>
        public QueryResponseResult(HttpStatusCode httpStatusCode, String message, int totalCount, List<T> list)
        {
            HttpStatusCode = httpStatusCode;
            Message = message;
            TotalCount = totalCount;
            List = list;
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="value"></param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> DeserializeObject(string value)
        {
            return JsonConvert.DeserializeObject<QueryResponseResult<T>>(value);
        }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <returns>json字符串</returns>
        public String ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Success标志
        /// </summary>
        /// <param name="totalCount">查询总记录数</param>
        /// <param name="list">查询记录</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Success(int totalCount, List<T> list)
        {
            return new QueryResponseResult<T>(HttpStatusCode.OK, new String("OK"), totalCount, list);
        }

        /// <summary>
        /// Success标志
        /// </summary>
        /// <param name="list">查询记录</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Success(List<T> list)
        {
            return new QueryResponseResult<T>(HttpStatusCode.OK, new String("OK"), 0, list);
        }

        /// <summary>
        /// OK标志
        /// </summary>
        /// <param name="totalCount">查询总记录数</param>
        /// <param name="list">查询记录</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> OK(int totalCount, List<T> list)
        {
            return Success(totalCount, list);
        }

        /// <summary>
        /// OK标志
        /// </summary>
        /// <param name="list">查询记录</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> OK(List<T> list)
        {
            return Success(0, list);
        }

        /// <summary>
        /// Error标志
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Error()
        {
            return new QueryResponseResult<T>(HttpStatusCode.InternalServerError, new String("InternalServerError"), 0, null);
        }

        /// <summary>
        /// Error标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Error(String message)
        {
            return new QueryResponseResult<T>(HttpStatusCode.InternalServerError, message, 0, null);
        }

        /// <summary>
        /// BadRequest标志
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> BadRequest()
        {
            return new QueryResponseResult<T>(HttpStatusCode.BadRequest, new String("BadRequest"), 0, null);
        }

        /// <summary>
        /// BadRequest标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> BadRequest(String message)
        {
            return new QueryResponseResult<T>(HttpStatusCode.BadRequest, message, 0, null);
        }

        /// <summary>
        /// Unauthorized标志
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Unauthorized()
        {
            return new QueryResponseResult<T>(HttpStatusCode.Unauthorized, new String("Unauthorized"), 0, null);
        }

        /// <summary>
        /// Unauthorized标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Unauthorized(String message)
        {
            return new QueryResponseResult<T>(HttpStatusCode.Unauthorized, message, 0, null);
        }

        /// <summary>
        /// Forbidden标志
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Forbidden()
        {
            return new QueryResponseResult<T>(HttpStatusCode.Forbidden, new String("Forbidden"), 0, null);
        }

        /// <summary>
        /// Forbidden标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Forbidden(String message)
        {
            return new QueryResponseResult<T>(HttpStatusCode.Forbidden, message, 0, null);
        }

        /// <summary>
        /// NotFound标志
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> NotFound()
        {
            return new QueryResponseResult<T>(HttpStatusCode.NotFound, new String("NotFound"), 0, null);
        }

        /// <summary>
        /// NotFound标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> NotFound(String message)
        {
            return new QueryResponseResult<T>(HttpStatusCode.NotFound, message, 0, null);
        }

        /// <summary>
        /// NoContent标志
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> NoContent()
        {
            return new QueryResponseResult<T>(HttpStatusCode.NoContent, new String("NoContent"), 0, null);
        }

        /// <summary>
        /// NoContent标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> NoContent(String message)
        {
            return new QueryResponseResult<T>(HttpStatusCode.NoContent, message, 0, null);
        }

        /// <summary>
        /// Null标志
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        public static QueryResponseResult<T> Null()
        {
            return new QueryResponseResult<T>(HttpStatusCode.OK, new String("OK"), 0, null);
        }
    }
}
