﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Newtonsoft.Json;
using System;
using System.Net;

namespace RuYiAdmin.Net.CommonInfrastructure.Models
{
    /// <summary>
    /// 动作执行结果
    /// </summary>
    public class ActionResponseResult
    {
        /// <summary>
        /// HttpStatusCode
        /// </summary>
        public HttpStatusCode HttpStatusCode { get; set; }

        /// <summary>
        /// 执行消息
        /// </summary>
        public String Message { get; set; }

        /// <summary>
        /// 执行对象
        /// </summary>
        public Object Object { get; set; }

        /// <summary>
        /// 无参构造函数
        /// </summary>
        public ActionResponseResult()
        {

        }

        /// <summary>
        /// 带参构造函数
        /// </summary>
        /// <param name="httpStatusCode">Http状态码</param>
        /// <param name="message">消息内容</param>
        /// <param name="obj">返回对象</param>
        public ActionResponseResult(HttpStatusCode httpStatusCode, String message, Object obj)
        {
            HttpStatusCode = httpStatusCode;
            Message = message;
            Object = obj;
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="value">输入</param>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult DeserializeObject(string value)
        {
            return JsonConvert.DeserializeObject<ActionResponseResult>(value);
        }

        /// <summary>
        /// 序列化
        /// </summary>
        /// <returns>json字符串</returns>
        public String ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Success标志
        /// </summary>
        /// <param name="obj">返回对象</param>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult Success(Object obj)
        {
            return new ActionResponseResult(HttpStatusCode.OK, new String("OK"), obj);
        }

        /// <summary>
        /// OK标志
        /// </summary>
        /// <param name="obj">返回对象</param>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult OK(Object obj)
        {
            return Success(obj);
        }

        /// <summary>
        /// OK标志
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult OK()
        {
            return new ActionResponseResult(HttpStatusCode.OK, new String("OK"), null);
        }

        /// <summary>
        /// BadRequest标志
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult BadRequest()
        {
            return new ActionResponseResult(HttpStatusCode.BadRequest, new String("BadRequest"), null);
        }

        /// <summary>
        /// BadRequest标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult BadRequest(String message)
        {
            return new ActionResponseResult(HttpStatusCode.BadRequest, message, null);
        }

        /// <summary>
        /// Unauthorized标志
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult Unauthorized()
        {
            return new ActionResponseResult(HttpStatusCode.Unauthorized, new String("Unauthorized"), null);
        }

        /// <summary>
        /// Unauthorized标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult Unauthorized(String message)
        {
            return new ActionResponseResult(HttpStatusCode.Unauthorized, message, null);
        }

        /// <summary>
        /// Forbidden标志
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult Forbidden()
        {
            return new ActionResponseResult(HttpStatusCode.Forbidden, new String("Forbidden"), null);
        }

        /// <summary>
        /// Forbidden标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult Forbidden(String message)
        {
            return new ActionResponseResult(HttpStatusCode.Forbidden, message, null);
        }

        /// <summary>
        /// NotFound标志
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult NotFound()
        {
            return new ActionResponseResult(HttpStatusCode.NotFound, new String("NotFound"), null);
        }

        /// <summary>
        /// NotFound标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult NotFound(String message)
        {
            return new ActionResponseResult(HttpStatusCode.NotFound, message, null);
        }

        /// <summary>
        /// NoContent标志
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult NoContent()
        {
            return new ActionResponseResult(HttpStatusCode.NoContent, new String("NoContent"), null);
        }

        /// <summary>
        /// NoContent标志
        /// </summary>
        /// <param name="message">描述语</param>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult NoContent(String message)
        {
            return new ActionResponseResult(HttpStatusCode.NoContent, message, null);
        }

        /// <summary>
        /// Null标志
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        public static ActionResponseResult Null()
        {
            return new ActionResponseResult(HttpStatusCode.OK, new String("OK"), null);
        }
    }
}
