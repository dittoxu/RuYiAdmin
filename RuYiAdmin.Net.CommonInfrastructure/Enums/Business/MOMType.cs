﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Enums.Business
{
    /// <summary>
    /// 消息中间件类型
    /// </summary>
    public enum MomType
    {
        /// <summary>
        /// ActiveMQ
        /// </summary>
        ActiveMQ,

        /// <summary>
        /// RabbitMQ
        /// </summary>
        RabbitMQ
    }
}
