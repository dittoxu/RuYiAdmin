﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Enums.Business
{
    /// <summary>
    /// 阅读状态
    /// </summary>
    public enum ReadingStatus
    {
        /// <summary>
        /// 未读
        /// </summary>
        Unread,

        /// <summary>
        /// 已读
        /// </summary>
        Read
    }
}
