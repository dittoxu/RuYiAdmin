﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Enums.Business
{
    /// <summary>
    /// 删除类型枚举
    /// </summary>
    public enum DeletionType
    {
        /// <summary>
        /// 未删除的
        /// </summary>
        Undeleted,

        /// <summary>
        /// 删除的
        /// </summary>
        Deleted
    }
}
