﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Enums.Business
{
    /// <summary>
    /// 菜单类型
    /// </summary>
    public enum MenuType
    {
        /// <summary>
        /// 菜单
        /// </summary>
        Menu,

        /// <summary>
        /// 按钮
        /// </summary>
        Button,

        /// <summary>
        /// 视图
        /// </summary>
        View
    }
}
