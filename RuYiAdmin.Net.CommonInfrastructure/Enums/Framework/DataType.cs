﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Enums.Framework
{
    // <summary>
    /// 数据类型
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// 字符串类型
        /// </summary>
        String,

        /// <summary>
        /// 日期
        /// </summary>
        Date,

        /// <summary>
        /// 时间类型
        /// </summary>
        DateTime,

        /// <summary>
        /// GUID
        /// </summary>
        Guid,

        /// <summary>
        /// 整型
        /// </summary>
        Int,

        /// <summary>
        /// 浮点型
        /// </summary>
        Double
    }
}
