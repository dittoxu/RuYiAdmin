﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;
using System.IO;
using Tesseract;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils
{
    /// <summary>
    /// OCR工具类
    /// </summary>
    public static class RuYiOCRUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        public static string Recognize(string imagePath)
        {
            string result = string.Empty;

            using (var ocr = new TesseractEngine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                "Libraries/Tesseract"), "chi_sim", EngineMode.Default))
            {
                var pix = Pix.LoadFromFile(imagePath);
                using (var page = ocr.Process(pix))
                {
                    string text = page.GetText();
                    if (!string.IsNullOrEmpty(text))
                    {
                        result = text;
                    }
                }
            }

            return result;
        }
    }
}
