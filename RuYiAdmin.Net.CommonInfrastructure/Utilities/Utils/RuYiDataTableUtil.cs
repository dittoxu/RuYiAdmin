﻿using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils
{
    /// <summary>
    /// DataTable帮助工具类
    /// </summary>
    public static class RuYiDataTableUtil
    {
        /// <summary>
        /// 将对象转化为DataTable
        /// </summary>
        /// <param name="obj">object</param>
        /// <param name="datetimeFormat">日期是否格式化</param>
        /// <returns>DataTable</returns>
        public static DataTable ConvertObjectToDataTable(object obj, bool datetimeFormat = true)
        {
            try
            {
                Type t;

                if (obj.GetType().IsGenericType)
                {
                    t = obj.GetType().GetGenericTypeDefinition();
                }
                else
                {
                    t = obj.GetType();
                }

                if (t == typeof(List<>) || t == typeof(IEnumerable<>))
                {
                    IEnumerable<object> lstenum = obj as IEnumerable<object>;
                    if (lstenum.Count() > 0)
                    {
                        //创建列
                        var ob1 = lstenum.GetEnumerator();
                        ob1.MoveNext();

                        DataTable dt = new DataTable();
                        dt.TableName = ob1.Current.GetType().Name;
                        foreach (var item in ob1.Current.GetType().GetProperties())
                        {
                            dt.Columns.Add(new DataColumn() { ColumnName = item.Name });
                        }

                        //装载数据
                        foreach (var item in lstenum)
                        {
                            DataRow row = dt.NewRow();
                            foreach (var sub in item.GetType().GetProperties())
                            {
                                var value = sub.GetValue(item, null);
                                if (datetimeFormat)
                                {
                                    if (sub.PropertyType.Equals(typeof(DateTime)))
                                    {
                                        row[sub.Name] = value == null ? DBNull.Value : ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    }
                                    else if (sub.PropertyType.Equals(typeof(DateTime?)))
                                    {
                                        row[sub.Name] = value == null ? DBNull.Value : ((DateTime)(DateTime?)value).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    }
                                    else
                                    {
                                        row[sub.Name] = value == null ? DBNull.Value : value;
                                    }
                                }
                                else
                                {
                                    row[sub.Name] = value == null ? DBNull.Value : value;
                                }
                            }
                            dt.Rows.Add(row);
                        }

                        return dt;
                    }
                }
                else if (t == typeof(DataTable))
                {
                    return (DataTable)obj;
                }
                else   //(t==typeof(Object))
                {
                    //创建列
                    DataTable dt = new DataTable();
                    dt.TableName = obj.GetType().Name;
                    foreach (var item in obj.GetType().GetProperties())
                    {
                        dt.Columns.Add(new DataColumn() { ColumnName = item.Name });
                    }

                    //装载数据
                    DataRow row = dt.NewRow();
                    foreach (var item in obj.GetType().GetProperties())
                    {
                        var value = item.GetValue(obj, null);
                        if (datetimeFormat)
                        {
                            if (item.PropertyType.Equals(typeof(DateTime)))
                            {
                                row[item.Name] = value == null ? DBNull.Value : ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss.fff");
                            }
                            else if (item.PropertyType.Equals(typeof(DateTime?)))
                            {
                                row[item.Name] = value == null ? DBNull.Value : ((DateTime)(DateTime?)value).ToString("yyyy-MM-dd HH:mm:ss.fff");
                            }
                            else
                            {
                                row[item.Name] = value == null ? DBNull.Value : value;
                            }
                        }
                        else
                        {
                            row[item.Name] = value == null ? DBNull.Value : value;
                        }
                    }
                    dt.Rows.Add(row);

                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw new RuYiAdminCustomException(ex.Message);
            }

            return null;
        }
    }
}
