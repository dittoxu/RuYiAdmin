﻿using Elasticsearch.Net;
using Nest;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System;
using System.Collections.Generic;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts
{
    /// <summary>
    /// ES NEST工具类
    /// </summary>
    public static class RuYiEsNestContext
    {
        /// <summary>
        /// Lazy对象
        /// </summary>
        private static readonly Lazy<ElasticClient> LazyInstance = new Lazy<ElasticClient>(() =>
        {
            if (RuYiGlobalConfig.ElasticsearchConfig.Uri.Contains(','))
            {
                var urls = new List<Uri>();
                foreach (var item in RuYiGlobalConfig.ElasticsearchConfig.Uri.Split(','))
                {
                    urls.Add(new Uri(item));
                }
                //创建连接池
                var connectionPool = new SniffingConnectionPool(urls);

                //创建连接设置
                var settings = new ConnectionSettings(connectionPool);
                settings.DefaultIndex(RuYiGlobalConfig.ElasticsearchConfig.DefaultIndex);

                if (!string.IsNullOrEmpty(RuYiGlobalConfig.ElasticsearchConfig.UserName)
                    &&
                    !string.IsNullOrEmpty(RuYiGlobalConfig.ElasticsearchConfig.Password))
                {
                    settings.BasicAuthentication(RuYiGlobalConfig.ElasticsearchConfig.UserName,
                                                 RuYiGlobalConfig.ElasticsearchConfig.Password);
                }

                var elasticClient = new ElasticClient(settings);
                return elasticClient;
            }
            else
            {
                //创建连接设置
                var settings = new ConnectionSettings(new Uri(RuYiGlobalConfig.ElasticsearchConfig.Uri));
                settings.DefaultIndex(RuYiGlobalConfig.ElasticsearchConfig.DefaultIndex);

                if (!string.IsNullOrEmpty(RuYiGlobalConfig.ElasticsearchConfig.UserName)
                    &&
                    !string.IsNullOrEmpty(RuYiGlobalConfig.ElasticsearchConfig.Password))
                {
                    settings.BasicAuthentication(RuYiGlobalConfig.ElasticsearchConfig.UserName,
                                                 RuYiGlobalConfig.ElasticsearchConfig.Password);
                }

                var elasticClient = new ElasticClient(settings);
                return elasticClient;
            }
        });

        /// <summary>
        /// 单例对象
        /// </summary>
        public static ElasticClient Instance { get { return LazyInstance.Value; } }

        /// <summary>
        /// 是否已创建
        /// </summary>
        public static bool IsInstanceCreated { get { return LazyInstance.IsValueCreated; } }
    }
}
