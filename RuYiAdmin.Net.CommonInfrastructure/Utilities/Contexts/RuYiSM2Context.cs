﻿using KYSharp.SM;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System.Text;

namespace RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts
{
    /// <summary>
    /// SM2会话工具
    /// </summary>
    public class RuYiSM2Context
    {
        /// <summary>
        /// 公钥加密明文
        /// </summary>
        /// <param name="plainText">明文</param>
        /// <returns>密文</returns>
        public static string Encrypt(string plainText)
        {
            return SM2Utils.Encrypt_Hex(RuYiGlobalConfig.SM2Config.PublicKey, plainText, Encoding.UTF8);
        }

        /// <summary>
        /// 私钥解密密文
        /// </summary>
        /// <param name="cipherText">密文</param>
        /// <returns>明文</returns>
        public static string Decrypt(string cipherText)
        {
            return SM2Utils.Decrypt_Hex(RuYiGlobalConfig.SM2Config.PrivateKey, cipherText, Encoding.UTF8);
        }
    }
}
