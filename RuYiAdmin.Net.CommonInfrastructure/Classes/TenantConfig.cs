﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Classes
{
    /// <summary>
    /// 租户配置模型
    /// </summary>
    public class TenantConfig
    {
        /// <summary>
        /// 租户编号
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// 数据库类型
        /// </summary>
        public int DbType { get; set; }

        /// <summary>
        /// 数据库连接串
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// 是否自动关闭
        /// </summary>
        public bool IsAutoCloseConnection { get; set; }
    }
}
