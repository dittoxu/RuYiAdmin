﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Classes
{
    /// <summary>
    /// 验证码
    /// </summary>
    public class CaptchaPicture
    {
        /// <summary>
        /// 编号
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// base64格式验证码
        /// </summary>
        public string CaptchaValue { get; set; }
    }
}
