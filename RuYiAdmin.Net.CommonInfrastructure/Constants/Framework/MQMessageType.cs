﻿using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Constants.Framework
{
    /// <summary>
    /// MQ消息类型
    /// </summary>
    public class MQMessageType
    {
        /// <summary>
        /// 队列
        /// </summary>
        public const String QUEUE = "queue";

        /// <summary>
        /// 主题
        /// </summary>
        public const String TOPIC = "topic";
    }
}
