﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.CommonInfrastructure.Constants.Framework
{
    /// <summary>
    /// 关键字
    /// </summary>
    public class Keywords
    {
        /// <summary>
        /// header头部Authorization
        /// </summary>
        public const String AUTHORIZATION = "Authorization";

        /// <summary>
        /// header头部RefreshToken
        /// </summary>
        public const String REFRESHTOKEN = "RefreshToken";

        /// <summary>
        /// header头部token
        /// </summary>
        public const String TOKEN = "token";

        /// <summary>
        /// JWT头部
        /// </summary>
        public const String BEARER = "Bearer ";

        /// <summary>
        /// 广播
        /// </summary>
        public const String BROADCAST = "Broadcast";

        /// <summary>
        /// 下线
        /// </summary>
        public const String FORCELOGOUT = "ForceLogout";

        /// <summary>
        /// 美式英语
        /// </summary>
        public const String LANGUAGE_EN = "en-US";

        /// <summary>
        /// 俄语
        /// </summary>
        public const String LANGUAGE_RU = "ru-RU";
    }
}
