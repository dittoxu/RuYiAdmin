﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

namespace RuYiAdmin.Net.CommonInfrastructure.Constants.Framework
{
    /// <summary>
    /// SM4模式类型
    /// </summary>
    public class SM4ModelType
    {
        /// <summary>
        /// CBC
        /// </summary>
        public const string CBC = "CBC";

        /// <summary>
        /// ECB
        /// </summary>
        public const string ECB = "ECB";
    }
}
