﻿using System;

namespace RuYiAdmin.Net.CommonInfrastructure.Constants.System
{
    /// <summary>
    /// 系统异常提示信息
    /// </summary>
    public class ExceptionMessage
    {
        /// <summary>
        /// 机构删除异常提示信息
        /// </summary>
        public const String DeleteOrgExceptionMessage = "org contains users or sub orgs,can not be deleted.";

        /// <summary>
        /// 用户名已存在异常提示信息
        /// </summary>
        public const String DuplicatedLogonNameExceptionMessage = "logon name has existed";

        /// <summary>
        /// 无效用户异常提示信息
        /// </summary>
        public const String InvalidUserExceptionMessage = "user is invalid";

        /// <summary>
        /// 无效用户名异常提示信息
        /// </summary>
        public const String NotNullUserNameExceptionMessage = "user name can not be null";

        /// <summary>
        /// 登录次数限制异常提示信息
        /// </summary>
        public const String LogonCountLimitedExceptionMessage = "logon count limited";

        /// <summary>
        /// 无效验证码异常提示信息
        /// </summary>
        public const String InvalidCaptchaExceptionMessage = "captcha is invalid";

        /// <summary>
        /// 无效口令异常提示信息
        /// </summary>
        public const String InvalidTokenExceptionMessage = "token is invalid";

        /// <summary>
        /// 无效同步账号异常提示信息
        /// </summary>
        public const String InvalidAccountExceptionMessage = "account is invalid";

        /// <summary>
        /// 无效盐异常提示信息
        /// </summary>
        public const String InvalidSaltExceptionMessage = "salt is invalid";

        /// <summary>
        /// 无效模型异常提示信息
        /// </summary>
        public const String InvalidModelStateExceptionMessage = "model state is invalid";

        /// <summary>
        /// 模块删除异常提醒信息
        /// </summary>
        public const String DeleteModuleExceptionMessage = "module has been used,can not be deleted.";

        /// <summary>
        /// 菜单删除异常提示信息
        /// </summary>
        public const String DeleteMenuExceptionMessage = "menu contains sub items,can not be deleted.";

        /// <summary>
        /// 角色删除异常提示信息
        /// </summary>
        public const String DeleteRoleExceptionMessage = "role has been used,can not be deleted.";

        /// <summary>
        /// 字典删除异常提示信息
        /// </summary>
        public const String DeleteCodeTableExceptionMessage = "code contains sub items,can not be deleted.";

        /// <summary>
        /// 导入配置删除异常提示信息
        /// </summary>
        public const String DeleteImportConfigExceptionMessage = "configuration contains sub items,can not be deleted.";

        /// <summary>
        /// 文件不存在异常提示信息
        /// </summary>
        public const String FileNotExistedExceptionMessage = "file is not existed";

        /// <summary>
        /// 脏数据读写异常提示信息
        /// </summary>
        public const String DirtyDataExceptionMessage = "data is dirty";

        /// <summary>
        /// 数据库未支持异常提示信息
        /// </summary>
        public const String DatabaseNotSupportedExceptionMessage = "database has not supported yet";

        /// <summary>
        /// 无效Cron表达式异常提示信息
        /// </summary>
        public const String InvalidCronExpressionExceptionMessage = "invalid cron expression";

        /// <summary>
        /// 停止作业异常提示信息
        /// </summary>
        public const String StopJobExceptionMessage = "job is running,please stop it first.";

        /// <summary>
        /// 鉴权异常提示信息
        /// </summary>
        public const String NotNullPermissionExceptionMessage = "permission can not be null";

        /// <summary>
        /// 无效JwtToken异常提示信息
        /// </summary>
        public const String UnauthorizedJwtTokenExceptionMessage = "unauthorized jwt token";

        /// <summary>
        /// 无效Token异常提示信息
        /// </summary>
        public const String NeccesarryTokenExceptionMessage = "token is neccesarry";
    }
}
