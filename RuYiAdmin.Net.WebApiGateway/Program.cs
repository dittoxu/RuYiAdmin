using Microsoft.OpenApi.Models;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Configuration.SetBasePath(Directory.GetCurrentDirectory());
builder.Configuration.AddJsonFile("appsettings.json", true, true);
builder.Configuration.AddJsonFile("ocelot.json", true, true);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "RuYiAdmin ASP.NET Core Gateway API Documentation",
        Description = "如意Admin，好框架，用心造。匠心独具，成就一代经典。品质优雅，注定恒久流传。",
        Contact = new OpenApiContact()
        {
            Name = "作者：不老的传说",
            Url = new Uri("https://gitee.com/pang-mingjun/RuYiAdmin"),
            Email = "983810803@qq.com"
        },
        License = new OpenApiLicense()
        {
            Name = "项目协议：Apache License 2.0",
            Url = new Uri("https://gitee.com/pang-mingjun/RuYiAdmin/blob/master/LICENSE")
        },
        Version = "V-1.0"
    });
});

builder.Logging.ClearProviders().AddConsole();
builder.Services.AddOcelot();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseOcelot().Wait();

app.Run();
