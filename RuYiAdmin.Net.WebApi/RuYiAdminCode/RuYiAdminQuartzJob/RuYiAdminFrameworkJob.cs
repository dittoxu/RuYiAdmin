﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.Extensions.Logging;
using Quartz;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using System;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminQuartzJob
{
    [DisallowConcurrentExecution]
    public class RuYiAdminFrameworkJob : IJob
    {
        private readonly ILogger<RuYiAdminFrameworkJob> _logger;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logger"></param>
        public RuYiAdminFrameworkJob(ILogger<RuYiAdminFrameworkJob> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 执行框架作业
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext context)
        {
            string time = DateTime.Now.ToString("HH:mm");
            if (time.Equals("04:00"))
            {
                //清理临时文件、释放服务器存储空间
                RuYiFileContext.ClearDirectory(RuYiGlobalConfig.DirectoryConfig.GetTempPath());
            }

            _logger.LogInformation("RuYiAdmin Framework Job Executed!");

            return Task.CompletedTask;
        }
    }
}