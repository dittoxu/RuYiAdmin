﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using System;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminClass
{
    [Serializable]
    public class JwtAuthentication
    {
        /// <summary>
        /// 登录名
        /// </summary>
        [Required]
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// 盐
        /// </summary>
        [Required]
        public string Salt { get; set; }

        /// <summary>
        /// 获取jwt口令
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns>JwtSecurityToken</returns>
        public static JwtSecurityToken GetJwtSecurityToken(string userName)
        {
            //创建claim
            var authClaims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub,userName),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
               };
            IdentityModelEventSource.ShowPII = true;
            //签名秘钥 
            var ecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(RuYiGlobalConfig.JwtSettings.SecurityKey));
            var token = new JwtSecurityToken(
                   issuer: RuYiGlobalConfig.JwtSettings.Issuer,
                   audience: RuYiGlobalConfig.JwtSettings.Audience,
                   expires: DateTime.Now.AddMinutes(RuYiGlobalConfig.JwtSettings.TokenExpiration),
                   claims: authClaims,
                   signingCredentials: new SigningCredentials(ecurityKey, SecurityAlgorithms.HmacSha256)
                   );
            return token;
        }
    }
}
