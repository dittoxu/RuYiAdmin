﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessServiceExtension;
using System;
using System.IO;

namespace RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation
{
    /// <summary>
    /// 审计日志过滤器
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class LogAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 操作类型
        /// </summary>
        private OperationType OperationType { get; set; }

        /// <summary>
        /// 行为描述信息
        /// </summary>
        private string Description { get; set; }

        public LogAttribute(OperationType operationType, string description = null)
        {
            OperationType = operationType;
            Description = description;
        }

        public override async void OnResultExecuted(ResultExecutedContext context)
        {
            base.OnResultExecuted(context);

            //判断审计日志记录开关是否开启
            if (!RuYiGlobalConfig.LogConfig.IsEnabled)
            {
                return;
            }

            var token = context.HttpContext.GetToken();
            //获取用户
            var user = RuYiRedisContext.Get<SysUserDTO>(token);
            if (user == null)
            {
                context.Exception = new Exception(ExceptionMessage.InvalidTokenExceptionMessage);
                return;
            }

            #region 监控信息入库

            var log = SysLogServiceExtension.GetSysLog(context.HttpContext);

            //字段赋值
            log.OperationType = OperationType;
            if (!string.IsNullOrEmpty(Description))
            {
                log.Remark = Description;
            }

            #region 设置返回值

            var isFileStream = context.Result.GetType().Equals(typeof(Microsoft.AspNetCore.Mvc.FileStreamResult));
            var isFile = context.Result.GetType().Equals(typeof(Microsoft.AspNetCore.Mvc.FileResult));
            var forbidenResult = context.Result.GetType().Equals(typeof(Microsoft.AspNetCore.Mvc.ForbidResult));
            if (!isFileStream && !isFile && !forbidenResult)
            {
                var result = JsonConvert.SerializeObject(((Microsoft.AspNetCore.Mvc.ObjectResult)context.Result).Value);
                if (result != null)
                {
                    if (result.Length > 2000)
                    {
                        log.Result = result.Substring(0, 2000) + "...";
                        var monitoringLogsPath = string.Join(string.Empty, RuYiGlobalConfig.DirectoryConfig.GetMonitoringLogsPath(), "/" + log.Id + ".txt");//返回结果落盘
                        var file = new FileStream(monitoringLogsPath, FileMode.Create);
                        byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(result);
                        file.Write(byteArray, 0, byteArray.Length);
                        file.Flush();
                        file.Close();
                    }
                    else
                    {
                        log.Result = result;
                    }
                }
            }

            #endregion

            //记录审计日志
            await log.WriteAsync();

            #endregion
        }
    }
}
