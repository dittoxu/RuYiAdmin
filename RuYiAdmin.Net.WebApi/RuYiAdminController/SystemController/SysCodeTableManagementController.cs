﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 数据字典管理控制器
    /// </summary>
    public class SysCodeTableManagementController : RuYiAdminBaseController<SysCodeTable>
    {
        #region 属性及构造函数

        /// <summary>
        /// 数据字典接口实例
        /// </summary>
        private readonly ISysCodeTableService CodeTableService;

        /// <summary>
        /// Redis接口实例
        /// </summary>
        private readonly IRedisService RedisService;

        /// <summary>
        /// AutoMapper实例
        /// </summary>
        private readonly IMapper AutoMapper;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="CodeTableService"></param>
        /// <param name="RedisService"></param>
        /// <param name="AutoMapper"></param>
        public SysCodeTableManagementController(ISysCodeTableService CodeTableService,
                                             IRedisService RedisService,
                                             IMapper AutoMapper) : base(CodeTableService)
        {
            this.CodeTableService = CodeTableService;
            this.RedisService = RedisService;
            this.AutoMapper = AutoMapper;
        }

        #endregion

        #region 查询字典列表

        /// <summary>
        /// 查询字典列表
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("code:query:list")]
        public async Task<IActionResult> Post()
        {
            var actionResponseResult = await this.CodeTableService.GetCodeTreeNodes();
            return Ok(actionResponseResult);
        }

        #endregion

        #region 获取字典信息

        /// <summary>
        /// 获取字典信息
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{id}")]
        [Log(OperationType.QueryEntity)]
        [Permission("code:query:list")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var codes = await this.RedisService.GetAsync<List<SysCodeTableDTO>>(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName);
            var actionResponseResult = ActionResponseResult.Success(codes.Where(t => t.Id == id).FirstOrDefault());
            return Ok(actionResponseResult);
        }

        #endregion

        #region 新增字典信息

        /// <summary>
        /// 新增字典信息
        /// </summary>
        /// <param name="codeTable">字典对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        [Permission("code:add:entity")]
        public async Task<IActionResult> Add([FromBody] SysCodeTable codeTable)
        {
            var actionResponseResult = await this.CodeTableService.AddAsync(codeTable);

            //数据一致性维护
            var codes = await this.RedisService.GetAsync<List<SysCodeTableDTO>>(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName);
            var codeTableDTO = this.AutoMapper.Map<SysCodeTableDTO>(codeTable);
            codes.Add(codeTableDTO);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName, codes, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 修改字典信息

        /// <summary>
        /// 修改字典信息
        /// </summary>
        /// <param name="codeTable">字典对象</param>
        /// <returns>IActionResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        [Permission("code:edit:entity")]
        public async Task<IActionResult> Put([FromBody] SysCodeTable codeTable)
        {
            var actionResponseResult = await this.CodeTableService.UpdateAsync(codeTable);

            //数据一致性维护
            var codes = await this.RedisService.GetAsync<List<SysCodeTableDTO>>(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName);
            var code = codes.Where(t => t.Id == codeTable.Id).FirstOrDefault();
            codes.Remove(code);
            var codeTableDTO = this.AutoMapper.Map<SysCodeTableDTO>(codeTable);
            codes.Add(codeTableDTO);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName, codes, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 删除字典信息

        /// <summary>
        /// 删除字典信息
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{id}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("code:del:entities")]
        public async Task<IActionResult> Delete(Guid id)
        {
            //字典删除检测
            await DeleteCheck(id.ToString());

            //删除字典信息
            var actionResponseResult = await this.CodeTableService.DeleteAsync(id);

            //数据一致性维护
            var codes = await this.RedisService.GetAsync<List<SysCodeTableDTO>>(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName);
            var code = codes.Where(t => t.Id == id).FirstOrDefault();
            codes.Remove(code);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName, codes, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 批量删除字典信息

        /// <summary>
        /// 批量删除字典信息
        /// </summary>
        /// <param name="ids">数组串</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{ids}")]
        [Log(OperationType.DeleteEntity)]
        [Permission("code:del:entities")]
        public async Task<IActionResult> DeleteRange(string ids)
        {
            //字典删除检测
            await DeleteCheck(ids);

            //删除字典信息
            var array = RuYiStringUtil.GetGuids(ids);
            var actionResponseResult = await this.CodeTableService.DeleteRangeAsync(array);

            //数据一致性维护
            var codes = await this.RedisService.GetAsync<List<SysCodeTableDTO>>(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName);
            foreach (var item in array)
            {
                var code = codes.Where(t => t.Id == item).FirstOrDefault();
                codes.Remove(code);
            }
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName, codes, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 获取字典子集

        /// <summary>
        /// 获取字典子集
        /// </summary>
        /// <param name="code">字典编码</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{code}")]
        [Log(OperationType.QueryList)]
        //[Permission("code:query:list")]
        public async Task<IActionResult> GetChildrenByCode(string code)
        {
            ActionResponseResult actionResponseResult = null;

            var codes = await this.RedisService.GetAsync<List<SysCodeTableDTO>>(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName);
            var root = codes.Where(t => t.Code.Equals(code)).FirstOrDefault();

            if (root != null)
            {
                actionResponseResult = ActionResponseResult.Success(codes.Where(t => t.ParentId.Equals(root.Id)).OrderBy(t => t.SerialNumber).ToList());
            }
            else
            {
                actionResponseResult = ActionResponseResult.Null();
            }

            return Ok(actionResponseResult);
        }

        #endregion

        #region 字典删除检测

        /// <summary>
        /// 字典删除检测
        /// </summary>
        /// <param name="ids">编号数组</param>
        /// <returns></returns>
        private async Task DeleteCheck(string ids)
        {
            var codes = await this.RedisService.GetAsync<List<SysCodeTableDTO>>(RuYiGlobalConfig.SystemCacheConfig.CodeTableCacheName);

            var array = RuYiStringUtil.GetGuids(ids);
            foreach (var item in array)
            {
                var sysCodes = codes.Where(t => t.ParentId == item).ToList();
                if (sysCodes.Count > 0)
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.DeleteCodeTableExceptionMessage);
                }
            }
        }

        #endregion
    }
}
