﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.HSSF.UserModel;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Extensions.Business;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.SessionScope;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 角色管理控制器
    /// </summary>
    public class SysRoleManagementController : RuYiAdminBaseController<SysRole>
    {
        #region 属性及构造函数

        /// <summary>
        /// 角色服务接口实例
        /// </summary>
        private readonly ISysRoleService RoleService;

        /// <summary>
        /// 角色用户服务接口
        /// </summary>
        private readonly ISysRoleUserService RoleUserService;

        /// <summary>
        /// 角色菜单服务接口
        /// </summary>
        private readonly ISysRoleMenuService RoleMenuService;

        /// <summary>
        /// 导入配置服务接口
        /// </summary>
        private readonly ISysImportConfigService ImportConfigService;

        /// <summary>
        /// 角色机构服务接口
        /// </summary>
        private readonly ISysRoleOrgService RoleOrgService;

        /// <summary>
        /// Redis接口实例
        /// </summary>
        private readonly IRedisService RedisService;

        /// <summary>
        /// AutoMapper实例
        /// </summary>
        private readonly IMapper AutoMapper;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="RoleService">角色服务实例</param>
        /// <param name="RoleUserService">角色用户服务实例</param>
        /// <param name="RoleMenuService">角色菜单服务实例</param>
        /// <param name="ImportConfigService">导入配置服务实例</param>
        /// <param name="RoleOrgService">角色机构服务实例</param>
        /// <param name="RedisService">redis服务实例</param>
        /// <param name="AutoMapper">AutoMapper实例</param>
        public SysRoleManagementController(ISysRoleService RoleService,
                                           ISysRoleUserService RoleUserService,
                                           ISysRoleMenuService RoleMenuService,
                                           ISysImportConfigService ImportConfigService,
                                           ISysRoleOrgService RoleOrgService,
                                           IRedisService RedisService,
                                           IMapper AutoMapper) : base(RoleService)
        {
            this.RoleService = RoleService;
            this.RoleUserService = RoleUserService;
            this.RoleMenuService = RoleMenuService;
            this.ImportConfigService = ImportConfigService;
            this.RoleOrgService = RoleOrgService;
            this.RedisService = RedisService;
            this.AutoMapper = AutoMapper;
        }

        #endregion

        #region 查询角色列表

        /// <summary>
        /// 查询角色列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("role:query:list")]
        public async Task<IActionResult> Post(SearchCondition searchCondition)
        {
            var roles = await this.RedisService.GetAsync<List<SysRoleDTO>>(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName);

            //获取用户机构编号
            var orgId = RuYiSessionContext.GetUserOrgId(this.HttpContext);
            roles = roles.Where(r => r.OrgId == orgId).ToList();

            roles = roles.AsQueryable().Where(searchCondition.ConvertToLamdaExpression<SysRoleDTO>()).ToList();

            if (!string.IsNullOrEmpty(searchCondition.Sort))
            {
                roles = roles.Sort(searchCondition.Sort);
            }

            int count = roles.Count;
            roles = roles.Skip(searchCondition.PageIndex * searchCondition.PageSize).Take(searchCondition.PageSize).ToList();

            var actionResponseResult = QueryResponseResult<SysRoleDTO>.OK(count, roles);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 查询角色信息

        /// <summary>
        /// 查询角色信息
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{roleId}")]
        [Log(OperationType.QueryEntity)]
        [Permission("role:query:list")]
        public async Task<IActionResult> GetById(Guid roleId)
        {
            var roles = await this.RedisService.GetAsync<List<SysRoleDTO>>(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName);
            var actionResponseResult = ActionResponseResult.Success(roles.Where(t => t.Id == roleId).FirstOrDefault());
            return Ok(actionResponseResult);
        }

        #endregion

        #region 新增系统角色

        /// <summary>
        /// 新增系统角色
        /// </summary>
        /// <param name="role">角色对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.AddEntity)]
        [Permission("role:add:entity")]
        public async Task<IActionResult> Add([FromBody] SysRole role)
        {
            var actionResponseResult = await this.RoleService.AddAsync(role);

            var roleOrg = new SysRoleOrg();
            roleOrg.RoleId = role.Id;
            //获取用户机构编号
            var orgId = RuYiSessionContext.GetUserOrgId(this.HttpContext);
            roleOrg.OrgId = orgId;
            roleOrg.OwnerType = (int)RoleType.Own;
            await this.RoleOrgService.AddAsync(roleOrg);

            #region 数据一致性维护

            var roles = await this.RedisService.GetAsync<List<SysRoleDTO>>(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName);
            var roleDTO = this.AutoMapper.Map<SysRoleDTO>(role);
            roleDTO.OrgId = roleOrg.OrgId;
            roles.Add(roleDTO);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName, roles, -1);

            var roleOrgs = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);
            roleOrgs.Add(roleOrg);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName, roleOrgs, -1);

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region 编辑系统角色

        /// <summary>
        /// 编辑系统角色
        /// </summary>
        /// <param name="role">角色对象</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPut]
        [Log(OperationType.EditEntity)]
        [Permission("role:edit:entity")]
        public async Task<IActionResult> Put([FromBody] SysRole role)
        {
            var actionResponseResult = await this.RoleService.UpdateAsync(role);

            #region 数据一致性维护

            var roles = await this.RedisService.GetAsync<List<SysRoleDTO>>(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName);

            var old = roles.Where(t => t.Id == role.Id).FirstOrDefault();
            roles.Remove(old);//删除旧数据

            var roleDTO = this.AutoMapper.Map<SysRoleDTO>(role);
            roleDTO.OrgId = old.OrgId;
            roles.Add(roleDTO);//删除新数据

            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName, roles, -1);

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region 删除系统角色

        /// <summary>
        /// 删除系统角色
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{roleId}")]
        [Permission("role:del:entities")]
        [Log(OperationType.DeleteEntity)]
        public async Task<IActionResult> Delete(Guid roleId)
        {
            //删除检测
            await DeleteCheck(roleId.ToString());

            //删除角色
            var actionResponseResult = await this.RoleService.DeleteAsync(roleId);
            //删除角色与机构关系
            var roleOrgs = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);
            var sysRoleOrg = roleOrgs.Where(t => t.RoleId.Equals(roleId) && t.IsDel == (int)DeletionType.Undeleted && t.OwnerType == (int)RoleType.Own)
                .FirstOrDefault();
            await this.RoleOrgService.DeleteAsync(sysRoleOrg.Id);

            #region 数据一致性维护

            var roles = await this.RedisService.GetAsync<List<SysRoleDTO>>(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName);
            var old = roles.Where(t => t.Id == roleId).FirstOrDefault();
            roles.Remove(old);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName, roles, -1);

            roleOrgs.Remove(sysRoleOrg);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName, roleOrgs, -1);

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region 批量删除角色

        /// <summary>
        /// 批量删除角色
        /// </summary>
        /// <param name="ids">编号数组</param>
        /// <returns>ActionResponseResult</returns>
        [HttpDelete("{ids}")]
        [Permission("role:del:entities")]
        [Log(OperationType.DeleteEntity)]
        public async Task<IActionResult> DeleteRange(string ids)
        {
            //删除检测
            await DeleteCheck(ids);

            //删除角色
            var array = RuYiStringUtil.GetGuids(ids);
            var actionResponseResult = await this.RoleService.DeleteRangeAsync(array);
            //删除角色与机构关系  
            var roleOrgs = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);
            var delList = new List<SysRoleOrg>();
            foreach (var item in array)
            {
                var sysRoleOrg = roleOrgs.Where(t => t.RoleId.Equals(item) && t.IsDel == (int)DeletionType.Undeleted && t.OwnerType == (int)RoleType.Own)
                    .FirstOrDefault();
                delList.Add(sysRoleOrg);
            }
            await this.RoleOrgService.DeleteRangeAsync(delList.Select(t => t.Id).ToArray());

            #region 数据一致性维护

            var roles = await this.RedisService.GetAsync<List<SysRoleDTO>>(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName);
            foreach (var item in array)
            {
                var old = roles.Where(t => t.Id == item).FirstOrDefault();
                roles.Remove(old);
            }
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName, roles, -1);

            roleOrgs.RemoveRange(delList);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName, roleOrgs, -1);

            #endregion

            return Ok(actionResponseResult);
        }

        #endregion

        #region  角色授权用户

        /// <summary>
        /// 角色授权用户
        /// </summary>
        /// <param name="roleUsers">用户角色</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.PermissionAuthorization)]
        [Permission("role:grant:permission")]
        public async Task<IActionResult> GrantUserRole([FromBody] List<SysRoleUser> roleUsers)
        {
            var addList = new List<SysRoleUser>();
            var list = await this.RedisService.GetAsync<List<SysRoleUser>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName);

            if (roleUsers.Count > 0)
            {
                foreach (var item in roleUsers)
                {
                    var entity = list.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.UserId.Equals(item.UserId) && t.RoleId.Equals(item.RoleId))
                        .FirstOrDefault();
                    if (entity == null)
                    {
                        addList.Add(item);
                    }
                }
            }

            var actionResponseResult = await this.RoleUserService.AddListAsync(addList);

            //数据一致性维护
            list.AddRange(addList);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName, list, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 收回用户授权

        /// <summary>
        /// 收回用户授权
        /// </summary>
        /// <param name="roleUsers">用户角色</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.PermissionAuthorization)]
        [Permission("role:grant:permission")]
        public async Task<IActionResult> WithdrawGrantedUserRole([FromBody] List<SysRoleUser> roleUsers)
        {
            var delList = new List<SysRoleUser>();
            var list = await this.RedisService.GetAsync<List<SysRoleUser>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName);

            if (roleUsers.Count > 0)
            {
                foreach (var item in roleUsers)
                {
                    var entity = list.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.UserId.Equals(item.UserId) && t.RoleId.Equals(item.RoleId))
                        .FirstOrDefault();
                    if (entity != null)
                    {
                        delList.Add(entity);
                    }
                }
            }

            var actionResponseResult = await this.RoleUserService.DeleteRangeAsync(delList.Select(t => t.Id).ToArray());

            //数据一致性维护
            list.RemoveRange(delList);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName, list, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 查询角色授权用户

        /// <summary>
        /// 查询角色授权用户
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{roleId}")]
        [Log(OperationType.QueryList)]
        [Permission("role:grant:permission")]
        public async Task<IActionResult> GetGrantedUserRoles(Guid roleId)
        {
            var list = await this.RedisService.GetAsync<List<SysRoleUser>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName);
            list = list.Where(t => t.RoleId.Equals(roleId) && t.IsDel == (int)DeletionType.Undeleted).ToList();

            var actionResponseResult = ActionResponseResult.OK(list);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 角色关联菜单

        /// <summary>
        /// 角色关联菜单
        /// </summary>
        /// <param name="roleMenus">角色菜单</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.MenuAuthorization)]
        [Permission("role:relate:menus")]
        public async Task<IActionResult> GrantRoleMenus([FromBody] List<SysRoleMenu> roleMenus)
        {
            var roleMenuList = await this.RedisService.GetAsync<List<SysRoleMenu>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndMenuCacheName);

            if (roleMenus.Count > 0)
            {
                var list = roleMenuList.Where(t => t.RoleId.Equals(roleMenus.FirstOrDefault().RoleId) && t.IsDel == (int)DeletionType.Undeleted).ToList();
                var ids = list.Select(t => t.Id).ToArray();
                await this.RoleMenuService.DeleteRangeAsync(ids);

                roleMenuList.RemoveRange(list);
            }

            var actionResponseResult = await this.RoleMenuService.AddListAsync(roleMenus);

            //数据一致性维护
            roleMenuList.AddRange(roleMenus);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndMenuCacheName, roleMenuList, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 查询角色关联菜单

        /// <summary>
        /// 查询角色关联菜单
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{roleId}")]
        [Log(OperationType.QueryList)]
        [Permission("role:relate:menus")]
        public async Task<IActionResult> GetGrantedRoleMenus(Guid roleId)
        {
            var roleMenus = await this.RedisService.GetAsync<List<SysRoleMenu>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndMenuCacheName);
            roleMenus = roleMenus.Where(t => t.RoleId.Equals(roleId) && t.IsDel == (int)DeletionType.Undeleted).ToList();

            var actionResponseResult = ActionResponseResult.Success(roleMenus);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 查询角色继承机构

        /// <summary>
        /// 查询角色继承机构
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{roleId}")]
        [Log(OperationType.QueryList)]
        [Permission("role:delegate:permission")]
        public async Task<IActionResult> GetDelegatedRoleOrgs(Guid roleId)
        {
            var roleOrgs = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);
            roleOrgs = roleOrgs.Where(t => t.RoleId.Equals(roleId) && t.IsDel == (int)DeletionType.Undeleted && t.OwnerType.Equals((int)RoleType.Inherited)).ToList();

            var actionResponseResult = ActionResponseResult.OK(roleOrgs);
            return Ok(actionResponseResult);
        }

        #endregion

        #region 机构继承角色

        /// <summary>
        /// 机构继承角色
        /// </summary>
        /// <param name="roleOrgs">角色机构列表</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.DelegatePermission)]
        [Permission("role:delegate:permission")]
        public async Task<IActionResult> DelegateRoleOrgs([FromBody] List<SysRoleOrg> roleOrgs)
        {
            //新增列表
            var addList = new List<SysRoleOrg>();
            //查询列表
            var list = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);

            foreach (var item in roleOrgs)
            {
                if (list.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.RoleId.Equals(item.RoleId) &&
                                    t.OrgId.Equals(item.OrgId) && t.OwnerType.Equals((int)RoleType.Own)).Count() > 0)
                {
                    //自有角色，无需继承
                    continue;
                }

                var count = list.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.RoleId.Equals(item.RoleId) &&
                                            t.OrgId.Equals(item.OrgId) && t.OwnerType.Equals(item.OwnerType)).Count();
                //未继承该角色
                if (count == 0)
                {
                    addList.Add(item);
                }
            }

            //新增继承角色
            var actionResponseResult = await this.RoleOrgService.AddListAsync(addList);

            //数据一致性维护
            list.AddRange(addList);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName, list, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 收回机构继承角色

        /// <summary>
        /// 收回机构继承角色
        /// </summary>
        /// <param name="roleOrgs">角色机构列表</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.DeleteEntity)]
        [Permission("role:delegate:permission")]
        public async Task<IActionResult> WithdrawRoleOrgs([FromBody] List<SysRoleOrg> roleOrgs)
        {
            //删除列表
            var delList = new List<SysRoleOrg>();
            var delRoleUserList = new List<SysRoleUser>();

            //查询列表
            var list = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);
            var roleUsers = await this.RedisService.GetAsync<List<SysRoleUser>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName);
            var users = await this.RedisService.GetAsync<List<SysUserDTO>>(RuYiGlobalConfig.SystemCacheConfig.UserCacheName);

            foreach (var item in roleOrgs)
            {
                var entity = list.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.RoleId.Equals(item.RoleId) &&
                                             t.OrgId.Equals(item.OrgId) && t.OwnerType.Equals(item.OwnerType)).FirstOrDefault();
                if (entity != null)
                {
                    delList.Add(entity);

                    var userList = users.Where(t => t.OrgId == item.OrgId).ToList();
                    foreach (var user in userList)
                    {
                        var roleUser = roleUsers.Where(t => t.UserId == user.Id && t.RoleId == item.RoleId).FirstOrDefault();
                        if (roleUser != null)
                        {
                            delRoleUserList.Add(roleUser);
                        }
                    }
                }
            }

            //删除继承角色
            var actionResponseResult = await this.RoleOrgService.DeleteRangeAsync(delList.Select(t => t.Id).ToArray());
            await this.RoleUserService.DeleteRangeAsync(delRoleUserList.Select(t => t.Id).ToArray());

            //数据一致性维护
            list.RemoveRange(delList);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName, list, -1);

            roleUsers.RemoveRange(delRoleUserList);
            await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName, roleUsers, -1);

            return Ok(actionResponseResult);
        }

        #endregion

        #region 查询是否为继承角色

        /// <summary>
        /// 查询是否为继承角色
        /// </summary>
        /// <param name="roleId">角色编号</param>
        /// <returns>ActionResponseResult</returns>
        [HttpGet("{roleId}")]
        [Log(OperationType.QueryEntity)]
        [Permission("role:query:list")]
        public async Task<IActionResult> IsInheritedRole(Guid roleId)
        {
            var actionResponseResult = new ActionResponseResult();
            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");

            var roleOrgs = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);

            if (roleOrgs.Where(t => t.RoleId.Equals(roleId) && t.IsDel == (int)DeletionType.Undeleted &&
                                    t.OwnerType == (int)RoleType.Inherited
                                    && t.OrgId.Equals(RuYiSessionContext.GetUserOrgId(this.HttpContext))).Count() > 0)
            {
                actionResponseResult.Object = true;
            }
            else
            {
                actionResponseResult.Object = false;
            }

            return Ok(actionResponseResult);
        }

        #endregion

        #region 角色导出Excel

        /// <summary>
        /// 角色导出Excel
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Log(OperationType.ExportData)]
        [Permission("role:list:export")]
        public async Task<IActionResult> ExportExcel()
        {
            var roles = await this.RedisService.GetAsync<List<SysRoleDTO>>(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName);

            //获取用户机构编号
            var orgId = RuYiSessionContext.GetUserOrgId(this.HttpContext);
            roles = roles.Where(r => r.OrgId == orgId).ToList();
            roles = roles.OrderBy(t => t.SerialNumber).ToList();

            var stream = roles.Export().ToStream();
            stream.Position = 0;

            return File(stream, "application/octet-stream", "角色信息.xls");
        }

        #endregion

        #region Excel导入角色

        /// <summary>
        /// Excel导入角色
        /// </summary>
        /// <param name="file">excel文件</param>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.ImportData)]
        [Permission("role:list:import")]
        public async Task<IActionResult> Import([FromForm] IFormFile file)
        {
            var actionResponseResult = new ActionResponseResult();

            if (file != null)
            {
                #region 常规合法性校验

                //获取文件拓展名
                var extension = Path.GetExtension(file.FileName);
                //文件保存路径
                var filePath = RuYiFileContext.SaveFormFile(file);

                var configDTO = this.ImportConfigService.GetImportConfig("RoleImportConfig");
                if (configDTO == null)
                {
                    throw new RuYiAdminCustomException(WarnningMessage.ImportConfigNotFindMessage);
                }
                configDTO.ExcelPath = filePath;

                //常规合法性校验
                var errorCount = configDTO.ValidationDetecting();

                #endregion

                if (errorCount > 0)
                {
                    #region 常规校验出不合规项

                    actionResponseResult.Object = errorCount;
                    actionResponseResult.Message = configDTO.ExcelPath.Replace(extension, "").Split('/')[1];

                    #endregion
                }
                else
                {
                    #region 常规业务性校验

                    var roles = await this.RedisService.GetAsync<List<SysRoleDTO>>(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName);

                    //获取用户机构编号
                    var orgId = RuYiSessionContext.GetUserOrgId(this.HttpContext);
                    var currentRoles = roles.Where(r => r.OrgId == orgId).ToList();

                    var xlxStream = new FileStream(configDTO.ExcelPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    var workbook = new HSSFWorkbook(xlxStream);
                    var worksheet = workbook.GetSheetAt(0);

                    for (var i = configDTO.StartRow; i <= worksheet.LastRowNum; i++)
                    {
                        var value = worksheet.GetRow(i).GetCell(1).GetCellValue().Trim();
                        if (!string.IsNullOrEmpty(value) && currentRoles.Where(t => t.RoleName.Equals(value)).Count() > 0)
                        {
                            errorCount++;
                            worksheet.SetCellComment(i, 1, "角色名称重复！");
                        }
                    }

                    #endregion

                    if (errorCount > 0)
                    {
                        #region 业务校验出不合规项

                        var xlxPath = configDTO.ExcelPath;

                        configDTO.ExcelPath = workbook.SaveAsXlx(xlxPath);

                        xlxStream.Close();
                        System.IO.File.Delete(xlxPath);

                        actionResponseResult.Object = errorCount;
                        actionResponseResult.Message = configDTO.ExcelPath.Replace(extension, "").Split('/')[1];

                        #endregion
                    }
                    else
                    {
                        #region 执行业务导入

                        var roleList = new List<SysRole>();
                        var roleOrgList = new List<SysRoleOrg>();

                        var list = worksheet.ToList<SysRole>(configDTO.StartRow, configDTO.StartColumn);
                        foreach (var role in list)
                        {
                            //开启事务
                            await this.RoleService.UseTransactionAsync(async () =>
                            {
                                await this.RoleService.AddAsync(role, true, false);
                                roleList.Add(role);

                                var roleOrg = new SysRoleOrg();
                                roleOrg.RoleId = role.Id;
                                roleOrg.OrgId = orgId;
                                roleOrg.OwnerType = (int)RoleType.Own;
                                await this.RoleOrgService.AddAsync(roleOrg, true, false);
                                roleOrgList.Add(roleOrg);
                            });
                        }

                        #region 数据一致性维护

                        foreach (var role in roleList)
                        {
                            var roleDTO = this.AutoMapper.Map<SysRoleDTO>(role);
                            roleDTO.OrgId = orgId;
                            roles.Add(roleDTO);
                        }
                        await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleCacheName, roles, -1);

                        var roleOrgs = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);
                        roleOrgs.AddRange(roleOrgList);
                        await this.RedisService.SetAsync(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName, roleOrgs, -1);

                        #endregion

                        workbook.Close();
                        xlxStream.Close();

                        #endregion
                    }
                }
                actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            }
            else
            {
                actionResponseResult.HttpStatusCode = HttpStatusCode.NoContent;
                actionResponseResult.Message = new string("NoContent");
            }

            return Ok(actionResponseResult);
        }

        #endregion

        #region 角色删除检测

        /// <summary>
        /// 角色删除检测
        /// </summary>
        /// <param name="ids">编号数组</param>
        /// <returns>真假值</returns>
        private async Task DeleteCheck(string ids)
        {
            var roleUsers = await this.RedisService.GetAsync<List<SysRoleUser>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName);
            var roleMenus = await this.RedisService.GetAsync<List<SysRoleMenu>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndMenuCacheName);
            var roleOrgs = await this.RedisService.GetAsync<List<SysRoleOrg>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndOrgCacheName);

            var array = RuYiStringUtil.GetGuids(ids);
            foreach (var item in array)
            {
                if (roleUsers.Where(t => t.RoleId.Equals(item) && t.IsDel == (int)DeletionType.Undeleted).Count() > 0 ||
                    roleMenus.Where(t => t.RoleId.Equals(item) && t.IsDel == (int)DeletionType.Undeleted).Count() > 0 ||
                    roleOrgs.Where(t => t.RoleId.Equals(item) && t.IsDel == (int)DeletionType.Undeleted && t.OwnerType == (int)RoleType.Inherited).Count() > 0)
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.DeleteRoleExceptionMessage);
                }
            }
        }

        #endregion
    }
}
