﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Mvc;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminAnnotation;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminBase;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.SystemController
{
    /// <summary>
    /// 行政区域管理控制器
    /// </summary>
    public class SysAreaManagementController : RuYiAdminBaseController<SysArea>
    {
        #region 属性及构造函数

        /// <summary>
        /// 行政区域业务接口实例
        /// </summary>
        private readonly ISysAreaService AreaService;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="AreaService"></param>
        public SysAreaManagementController(ISysAreaService AreaService) : base(AreaService)
        {
            this.AreaService = AreaService;
        }

        #endregion

        #region 查询行政区域列表

        /// <summary>
        /// 查询行政区域列表
        /// </summary>
        /// <returns>ActionResponseResult</returns>
        [HttpPost]
        [Log(OperationType.QueryList)]
        [Permission("area:query:list")]
        public async Task<IActionResult> Post()
        {
            var actionResponseResult = await this.AreaService.GetAreaTreeNodes();
            return Ok(actionResponseResult);
        }

        #endregion
    }
}
