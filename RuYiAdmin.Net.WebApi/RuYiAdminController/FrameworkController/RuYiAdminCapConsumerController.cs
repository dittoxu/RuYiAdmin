﻿using DotNetCore.CAP;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.WebApi.RuYiAdminCode.RuYiAdminFilter;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RuYiAdmin.Net.WebApi.RuYiAdminController.FrameworkController
{
    [Authorize]
    [ActionAuthorization]
    [ApiController]
    [Route(RuYiGlobalConfig.RouteTemplate)]
    public class RuYiAdminCapConsumerController : ControllerBase
    {
        private readonly ILogger<RuYiAdminCapConsumerController> logger;

        public RuYiAdminCapConsumerController(ILogger<RuYiAdminCapConsumerController> logger)
        {
            this.logger = logger;
        }

        [NonAction]
        [CapSubscribe("RuYiAdmin.CapService.ShowTime")]
        public void ReceiveMessage(DateTime time, [FromCap] CapHeader header)
        {
            logger.LogInformation("message time is:" + time);
            logger.LogInformation("message firset header :" + header["my.header.first"]);
            logger.LogInformation("message second header :" + header["my.header.second"]);
        }
    }
}
