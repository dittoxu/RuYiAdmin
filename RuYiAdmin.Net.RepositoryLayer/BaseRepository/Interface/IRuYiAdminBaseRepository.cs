﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.RepositoryLayer.BaseRepository.Interface
{
    /// <summary>
    /// 数据库访问接口
    /// </summary>
    public interface IRuYiAdminBaseRepository<T> where T : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        List<T> GetPage(SearchCondition searchCondition, ref int totalCount);

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        List<T> GetList(SearchCondition searchCondition);

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        T GetById(Guid id);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        List<T> SqlQuery(SearchCondition searchCondition, string sqlKey, ref int totalCount);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <param name="strSQL">查询语句</param>
        /// <returns>查询结果</returns>
        List<T> SqlQuery(SearchCondition searchCondition, ref int totalCount, string strSQL);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录总数</param>
        /// <returns>查询结果</returns>
        List<TDTO> SqlQuery<TDTO>(SearchCondition searchCondition, string sqlKey, ref int totalCount) where TDTO : class, new();

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录总数</param>
        /// <param name="strSQL">sql语句</param>
        /// <returns>查询结果</returns>
        List<TDTO> SqlQuery<TDTO>(SearchCondition searchCondition, ref int totalCount, string strSQL) where TDTO : class, new();

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        List<T> DefaultSqlQuery(SearchCondition searchCondition, ref int totalCount);

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        List<TDTO> DefaultSqlQuery<TDTO>(SearchCondition searchCondition, ref int totalCount) where TDTO : class, new();

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>列表</returns>
        List<T> GetList();

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <returns>查询结果</returns>
        List<TDTO> GetList<TDTO>() where TDTO : class, new();

        /// <summary>
        /// 按表达式查询
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns>列表</returns>
        List<T> QueryByExpression(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>对象</returns>
        T AddEntity(T obj, bool create = true, bool transactional = true);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>集合</returns>
        List<T> AddList(List<T> list, bool create = true, bool transactional = true);

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>对象</returns>
        T UpdateEntity(T obj, bool transactional = true);

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>集合</returns>
        List<T> UpdateList(List<T> list, bool transactional = true);

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        bool DeleteEntity(Guid id, bool transactional = true);

        /// <summary>
        /// 逻辑批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志位</returns>
        bool DeleteRange(Guid[] ids, bool transactional = true);

        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        bool RemoveEntity(Guid id, bool transactional = true);

        /// <summary>
        /// 物理批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        bool RemoveRange(Guid[] ids, bool transactional = true);

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        int BulkCopy(List<T> list);

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns></returns>
        int PageBulkCopy(int pageSize, List<T> list);

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        int BulkUpdate(List<T> list);

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        int HugeDataBulkCopy(List<T> list);

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        int HugeDataBulkUpdate(List<T> list);

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        DbResult<bool> UseTransaction(Action action, Action<Exception> errorCallBack = null);

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        DbResult<TEntity> UseTransaction<TEntity>(Func<TEntity> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity;

        /// <summary>
        /// 通过key获取SQL语句
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>SQL语句</returns>
        String GetSqlByKey(String key);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        Task<List<T>> GetPageAsync(SearchCondition searchCondition, RefAsync<int> totalCount);

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        Task<List<T>> GetListAsync(SearchCondition searchCondition);

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        Task<T> GetByIdAsync(Guid id);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        Task<List<T>> SqlQueryAsync(SearchCondition searchCondition, string sqlKey, RefAsync<int> totalCount);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <param name="strSQL">sql键值</param>
        /// <returns>查询结果</returns>
        Task<List<T>> SqlQueryAsync(SearchCondition searchCondition, RefAsync<int> totalCount, string strSQL);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录总数</param>
        /// <returns>查询结果</returns>
        Task<List<TDTO>> SqlQueryAsync<TDTO>(SearchCondition searchCondition, string sqlKey, RefAsync<int> totalCount) where TDTO : class, new();

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录总数</param>
        /// <param name="strSQL">sql查询语句</param>
        /// <returns>查询结果</returns>
        Task<List<TDTO>> SqlQueryAsync<TDTO>(SearchCondition searchCondition, RefAsync<int> totalCount, string strSQL) where TDTO : class, new();

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        Task<List<T>> DefaultSqlQueryAsync(SearchCondition searchCondition, RefAsync<int> totalCount);

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        Task<List<TDTO>> DefaultSqlQueryAsync<TDTO>(SearchCondition searchCondition, RefAsync<int> totalCount) where TDTO : class, new();

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>列表</returns>
        Task<List<T>> GetListAsync();

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <returns>查询结果</returns>
        Task<List<TDTO>> GetListAsync<TDTO>() where TDTO : class, new();

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>对象</returns>
        Task<T> AddEntityAsync(T obj, bool create = true, bool transactional = true);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>集合</returns>
        Task<List<T>> AddListAsync(List<T> list, bool create = true, bool transactional = true);

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>对象</returns>
        Task<T> UpdateEntityAsync(T obj, bool transactional = true);

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>集合</returns>
        Task<List<T>> UpdateListAsync(List<T> list, bool transactional = true);

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        Task<bool> DeleteEntityAsync(Guid id, bool transactional = true);

        /// <summary>
        /// 逻辑批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志位</returns>
        Task<bool> DeleteRangeAsync(Guid[] ids, bool transactional = true);

        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        Task<bool> RemoveEntityAsync(Guid id, bool transactional = true);

        /// <summary>
        /// 物理批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        Task<bool> RemoveRangeAsync(Guid[] ids, bool transactional = true);

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        Task<int> BulkCopyAsync(List<T> list);

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns></returns>
        Task<int> PageBulkCopyAsync(int pageSize, List<T> list);

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        Task<int> BulkUpdateAsync(List<T> list);

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        Task<int> HugeDataBulkCopyAsync(List<T> list);

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        Task<int> HugeDataBulkUpdateAsync(List<T> list);

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        Task<DbResult<bool>> UseTransactionAsync(Func<Task> action, Action<Exception> errorCallBack = null);

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        Task<DbResult<TEntity>> UseTransactionAsync<TEntity>(Func<Task<TEntity>> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity;
    }
}
