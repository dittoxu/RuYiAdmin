﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.CommonInfrastructure.Classes;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.BaseRepository.Interface;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.RepositoryLayer.BaseRepository.Implementation
{
    /// <summary>
    /// 数据库访问基类
    /// </summary>
    public class RuYiAdminBaseRepository<T> : RuYiAdminDbScope, IRuYiAdminBaseRepository<T> where T : RuYiAdminBaseEntity, new()
    {
        #region 属性及构造函数

        /// <summary>
        /// 数据库上下文
        /// </summary>
        //private readonly Repository<T> Repository;

        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor context;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="context"></param>
        public RuYiAdminBaseRepository(IHttpContextAccessor context)
        {
            //this.Repository = new Repository<T>();
            this.context = context;
        }

        #endregion

        #region 基类公有方法

        #region 基类同步方法

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public List<T> GetPage(SearchCondition searchCondition, ref int totalCount)
        {
            searchCondition.AddDefaultSearchItem();
            var where = searchCondition.ConvertToLamdaExpression<T>();

            var list = RuYiDbContext.
                Queryable<T>().
                WhereIF(true, where).
                OrderByIF(!string.IsNullOrEmpty(searchCondition.Sort), searchCondition.Sort).
                ToPageList(searchCondition.PageIndex, searchCondition.PageSize, ref totalCount).
                ToList();

            return list;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public List<T> GetList(SearchCondition searchCondition)
        {
            searchCondition.AddDefaultSearchItem();
            var where = searchCondition.ConvertToLamdaExpression<T>();

            var list = RuYiDbContext.
                Queryable<T>().
                WhereIF(true, where).
                OrderByIF(!string.IsNullOrEmpty(searchCondition.Sort), searchCondition.Sort).
                ToList();

            return list;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        public T GetById(Guid id)
        {
            return RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(id));
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public List<T> SqlQuery(SearchCondition searchCondition, string sqlKey, ref int totalCount)
        {
            var sqlStr = this.GetQuerySQL(searchCondition, sqlKey);
            var list = this.GetPageList(searchCondition, sqlStr, ref totalCount);
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <param name="strSQL">查询语句</param>
        /// <returns>查询结果</returns>
        public List<T> SqlQuery(SearchCondition searchCondition, ref int totalCount, string strSQL)
        {
            var sqlStr = this.AssembleBasicQueryStatement(searchCondition, strSQL);
            var list = this.GetPageList(searchCondition, sqlStr, ref totalCount);
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录总数</param>
        /// <returns>查询结果</returns>
        public List<TDTO> SqlQuery<TDTO>(SearchCondition searchCondition, string sqlKey, ref int totalCount) where TDTO : class, new()
        {
            var sqlStr = this.GetQuerySQL(searchCondition, sqlKey);
            var list = this.GetPageList<TDTO>(searchCondition, sqlStr, ref totalCount);
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录总数</param>
        /// <param name="strSQL">查询语句</param>
        /// <returns>查询结果</returns>
        public List<TDTO> SqlQuery<TDTO>(SearchCondition searchCondition, ref int totalCount, string strSQL) where TDTO : class, new()
        {
            var sqlStr = this.AssembleBasicQueryStatement(searchCondition, strSQL);
            var list = this.GetPageList<TDTO>(searchCondition, sqlStr, ref totalCount);
            return list;
        }

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public List<T> DefaultSqlQuery(SearchCondition searchCondition, ref int totalCount)
        {
            var defaultQuerySQL = this.GetDefaultQuerySQL();
            var tableName = this.GetTableName();
            var strSql = string.Format(defaultQuerySQL, tableName);

            var sqlStr = this.AssembleBasicQueryStatement(searchCondition, strSql);
            var list = this.GetPageList(searchCondition, sqlStr, ref totalCount);
            return list;
        }

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public List<TDTO> DefaultSqlQuery<TDTO>(SearchCondition searchCondition, ref int totalCount) where TDTO : class, new()
        {
            var defaultQuerySQL = this.GetDefaultQuerySQL();
            var tableName = this.GetTableName();
            var strSql = string.Format(defaultQuerySQL, tableName);

            var sqlStr = this.AssembleBasicQueryStatement(searchCondition, strSql);
            var list = this.GetPageList<TDTO>(searchCondition, sqlStr, ref totalCount);
            return list;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>列表</returns>
        public List<T> GetList()
        {
            return RuYiDbContext.Queryable<T>().ToList();
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <returns>查询结果</returns>
        public List<TDTO> GetList<TDTO>() where TDTO : class, new()
        {
            return RuYiDbContext.Queryable<TDTO>().ToList();
        }

        /// <summary>
        /// 按表达式查询
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns>列表</returns>
        public List<T> QueryByExpression(Expression<Func<T, bool>> expression)
        {
            return RuYiDbContext.Queryable<T>().Where(expression).ToList();
        }

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>对象</returns>
        public T AddEntity(T obj, bool create = true, bool transactional = true)
        {
            //判断审计日志记录开关是否开启
            if (obj.GetType().Equals(typeof(SysLog)))
            {
                if (!RuYiGlobalConfig.LogConfig.IsEnabled)
                {
                    return obj;
                }
            }

            if (create)
            {
                obj.Create(context);
            }

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                RuYiDbContext.Insertable<T>(obj).ExecuteCommand();
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return obj;
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>集合</returns>
        public List<T> AddList(List<T> list, bool create = true, bool transactional = true)
        {
            //判断审计日志记录开关是否开启
            if (list != null && list.Count > 0 && list.FirstOrDefault().GetType().Equals(typeof(SysLog)))
            {
                if (!RuYiGlobalConfig.LogConfig.IsEnabled)
                {
                    return list;
                }
            }

            if (create)
            {
                foreach (var item in list)
                {
                    item.Create(context);
                }
            }

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                RuYiDbContext.Insertable<T>(list).ExecuteCommand();
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return list;
        }

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>对象</returns>
        public T UpdateEntity(T obj, bool transactional = true)
        {
            var entity = RuYiDbContext.Queryable<T>().WhereClassByPrimaryKey(obj).ToList().FirstOrDefault();
            if (!EntityVersionCheck(entity, obj))
            {
                throw new RuYiAdminCustomException(ExceptionMessage.DirtyDataExceptionMessage);
            }
            obj.Modify(context);

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                RuYiDbContext.Updateable<T>(obj).ExecuteCommand();
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return obj;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>集合</returns>
        public List<T> UpdateList(List<T> list, bool transactional = true)
        {
            foreach (var item in list)
            {
                var entity = RuYiDbContext.Queryable<T>().WhereClassByPrimaryKey(item).ToList().FirstOrDefault();
                if (!EntityVersionCheck(entity, item))
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.DirtyDataExceptionMessage);
                }
                item.Modify(context);
            }

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                RuYiDbContext.Updateable<T>(list).ExecuteCommand();
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return list;
        }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        public bool DeleteEntity(Guid id, bool transactional = true)
        {
            var result = false;

            var entity = RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(id));
            entity.Delete(context);

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                result = RuYiDbContext.Updateable<T>(entity).ExecuteCommand() > 0;
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 逻辑批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志位</returns>
        public bool DeleteRange(Guid[] ids, bool transactional = true)
        {
            var result = false;

            var list = new List<T>();
            foreach (var item in ids)
            {
                var entity = RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(item));
                entity.Delete(context);
                list.Add(entity);
            }

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                result = RuYiDbContext.Updateable<T>(list).ExecuteCommand() > 0;
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        public bool RemoveEntity(Guid id, bool transactional = true)
        {
            var result = false;

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                result = RuYiDbContext.Deleteable<T>().In(id).ExecuteCommand() > 0;
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 物理批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        public bool RemoveRange(Guid[] ids, bool transactional = true)
        {
            var result = false;

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                result = RuYiDbContext.Deleteable<T>().In(ids).ExecuteCommand() > 0;
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int BulkCopy(List<T> list)
        {
            return RuYiDbContext.Fastest<T>().BulkCopy(list);
        }

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int PageBulkCopy(int pageSize, List<T> list)
        {
            return RuYiDbContext.Fastest<T>().PageSize(pageSize).BulkCopy(list);
        }

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int BulkUpdate(List<T> list)
        {
            return RuYiDbContext.Fastest<T>().BulkUpdate(list);
        }

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int HugeDataBulkCopy(List<T> list)
        {
            var hugeData = RuYiDbContext.Storageable<T>(list).ToStorage();
            return hugeData.BulkCopy();
        }

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public int HugeDataBulkUpdate(List<T> list)
        {
            var hugeData = RuYiDbContext.Storageable<T>(list).ToStorage();
            return hugeData.BulkUpdate();
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public DbResult<bool> UseTransaction(Action action, Action<Exception> errorCallBack = null)
        {
            return RuYiDbContext.AsTenant().UseTran(action, errorCallBack);
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public DbResult<TEntity> UseTransaction<TEntity>(Func<TEntity> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity
        {
            return RuYiDbContext.AsTenant().UseTran(action, errorCallBack);
        }

        /// <summary>
        /// 时间版本比较
        /// </summary>
        /// <param name="entity">数据库实体</param>
        /// <param name="obj">业务实体</param>
        /// <returns>真假值</returns>
        public bool EntityVersionCheck(T entity, T obj)
        {
            return entity.VersionId == obj.VersionId;
        }

        /// <summary>
        /// 通过key获取SQL语句
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>SQL语句</returns>
        public String GetSqlByKey(String key)
        {
            String sqlStr = RuYiGlobalConfig.Configuration.GetSection(key).Value;
            return sqlStr;
        }

        #endregion

        #region 基类异步方法

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> GetPageAsync(SearchCondition searchCondition, RefAsync<int> totalCount)
        {
            searchCondition.AddDefaultSearchItem();
            var where = searchCondition.ConvertToLamdaExpression<T>();

            var list = await RuYiDbContext.
                Queryable<T>().
                WhereIF(true, where).
                OrderByIF(!string.IsNullOrEmpty(searchCondition.Sort), searchCondition.Sort).
                ToPageListAsync(searchCondition.PageIndex, searchCondition.PageSize, totalCount);

            return list;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> GetListAsync(SearchCondition searchCondition)
        {
            searchCondition.AddDefaultSearchItem();
            var where = searchCondition.ConvertToLamdaExpression<T>();

            var list = await RuYiDbContext.
                Queryable<T>().
                WhereIF(true, where).
                OrderByIF(!string.IsNullOrEmpty(searchCondition.Sort), searchCondition.Sort).
                ToListAsync();

            return list;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        public async Task<T> GetByIdAsync(Guid id)
        {
            return await RuYiDbContext.Queryable<T>().FirstAsync(t => t.Id.Equals(id));
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> SqlQueryAsync(SearchCondition searchCondition, string sqlKey, RefAsync<int> totalCount)
        {
            var sqlStr = this.GetQuerySQL(searchCondition, sqlKey);
            var list = await this.GetPageListAsync(searchCondition, sqlStr, totalCount);
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <param name="strSQL">查询语句</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> SqlQueryAsync(SearchCondition searchCondition, RefAsync<int> totalCount, string strSQL)
        {
            var sqlStr = this.AssembleBasicQueryStatement(searchCondition, strSQL);
            var list = await this.GetPageListAsync(searchCondition, sqlStr, totalCount);
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <param name="totalCount">记录总数</param>
        /// <returns>查询结果</returns>
        public async Task<List<TDTO>> SqlQueryAsync<TDTO>(SearchCondition searchCondition, string sqlKey, RefAsync<int> totalCount) where TDTO : class, new()
        {
            var sqlStr = this.GetQuerySQL(searchCondition, sqlKey);
            var list = await this.GetPageListAsync<TDTO>(searchCondition, sqlStr, totalCount);
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录总数</param>
        /// <param name="strSQL">sql查询语句</param>
        /// <returns>查询结果</returns>
        public async Task<List<TDTO>> SqlQueryAsync<TDTO>(SearchCondition searchCondition, RefAsync<int> totalCount, string strSQL) where TDTO : class, new()
        {
            var sqlStr = this.AssembleBasicQueryStatement(searchCondition, strSQL);
            var list = await this.GetPageListAsync<TDTO>(searchCondition, sqlStr, totalCount);
            return list;
        }

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public async Task<List<T>> DefaultSqlQueryAsync(SearchCondition searchCondition, RefAsync<int> totalCount)
        {
            var defaultQuerySQL = this.GetDefaultQuerySQL();
            var tableName = this.GetTableName();
            var strSql = string.Format(defaultQuerySQL, tableName);

            var sqlStr = this.AssembleBasicQueryStatement(searchCondition, strSql);
            var list = await this.GetPageListAsync(searchCondition, sqlStr, totalCount);
            return list;
        }

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        public async Task<List<TDTO>> DefaultSqlQueryAsync<TDTO>(SearchCondition searchCondition, RefAsync<int> totalCount) where TDTO : class, new()
        {
            var defaultQuerySQL = this.GetDefaultQuerySQL();
            var tableName = this.GetTableName();
            var strSql = string.Format(defaultQuerySQL, tableName);

            var sqlStr = this.AssembleBasicQueryStatement(searchCondition, strSql);
            var list = await this.GetPageListAsync<TDTO>(searchCondition, sqlStr, totalCount);
            return list;
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>列表</returns>
        public async Task<List<T>> GetListAsync()
        {
            return await RuYiDbContext.Queryable<T>().ToListAsync();
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <returns>查询结果</returns>
        public async Task<List<TDTO>> GetListAsync<TDTO>() where TDTO : class, new()
        {
            return await RuYiDbContext.Queryable<TDTO>().ToListAsync();
        }

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>对象</returns>
        public async Task<T> AddEntityAsync(T obj, bool create = true, bool transactional = true)
        {
            //判断审计日志记录开关是否开启
            if (obj.GetType().Equals(typeof(SysLog)))
            {
                if (!RuYiGlobalConfig.LogConfig.IsEnabled)
                {
                    return obj;
                }
            }

            if (create)
            {
                obj.Create(context);
            }

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                await RuYiDbContext.Insertable<T>(obj).ExecuteCommandAsync();
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return obj;
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>集合</returns>
        public async Task<List<T>> AddListAsync(List<T> list, bool create = true, bool transactional = true)
        {
            //判断审计日志记录开关是否开启
            if (list.FirstOrDefault().GetType().Equals(typeof(SysLog)))
            {
                if (!RuYiGlobalConfig.LogConfig.IsEnabled)
                {
                    return list;
                }
            }

            if (create)
            {
                foreach (var item in list)
                {
                    item.Create(context);
                }
            }

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                await RuYiDbContext.Insertable<T>(list).ExecuteCommandAsync();
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return list;
        }

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>对象</returns>
        public async Task<T> UpdateEntityAsync(T obj, bool transactional = true)
        {
            var entity = RuYiDbContext.Queryable<T>().WhereClassByPrimaryKey(obj).ToList().FirstOrDefault();
            if (!EntityVersionCheck(entity, obj))
            {
                throw new RuYiAdminCustomException(ExceptionMessage.DirtyDataExceptionMessage);
            }
            obj.Modify(context);

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                await RuYiDbContext.Updateable<T>(obj).ExecuteCommandAsync();
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return obj;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>集合</returns>
        public async Task<List<T>> UpdateListAsync(List<T> list, bool transactional = true)
        {
            foreach (var item in list)
            {
                var entity = RuYiDbContext.Queryable<T>().WhereClassByPrimaryKey(item).ToList().FirstOrDefault();
                if (!EntityVersionCheck(entity, item))
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.DirtyDataExceptionMessage);
                }
                item.Modify(context);
            }

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                await RuYiDbContext.Updateable<T>(list).ExecuteCommandAsync();
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return list;
        }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        public async Task<bool> DeleteEntityAsync(Guid id, bool transactional = true)
        {
            var result = false;

            var entity = RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(id));
            entity.Delete(context);

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                result = await RuYiDbContext.Updateable<T>(entity).ExecuteCommandAsync() > 0;
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 逻辑批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志位</returns>
        public async Task<bool> DeleteRangeAsync(Guid[] ids, bool transactional = true)
        {
            var result = false;

            var list = new List<T>();
            foreach (var item in ids)
            {
                var entity = RuYiDbContext.Queryable<T>().First(t => t.Id.Equals(item));
                entity.Delete(context);
                list.Add(entity);
            }

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                result = await RuYiDbContext.Updateable<T>(list).ExecuteCommandAsync() > 0;
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="id">编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        public async Task<bool> RemoveEntityAsync(Guid id, bool transactional = true)
        {
            var result = false;

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                result = await RuYiDbContext.Deleteable<T>().In(id).ExecuteCommandAsync() > 0;
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 物理批量删除
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>标志</returns>
        public async Task<bool> RemoveRangeAsync(Guid[] ids, bool transactional = true)
        {
            var result = false;

            try
            {
                if (transactional)
                {
                    RuYiDbContext.BeginTran();
                }
                result = await RuYiDbContext.Deleteable<T>().In(ids).ExecuteCommandAsync() > 0;
                if (transactional)
                {
                    RuYiDbContext.CommitTran();
                }
            }
            catch (Exception ex)
            {
                if (transactional)
                {
                    RuYiDbContext.RollbackTran();
                }
                throw new RuYiAdminCustomException(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> BulkCopyAsync(List<T> list)
        {
            return await RuYiDbContext.Fastest<T>().BulkCopyAsync(list);
        }

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> PageBulkCopyAsync(int pageSize, List<T> list)
        {
            return await RuYiDbContext.Fastest<T>().PageSize(pageSize).BulkCopyAsync(list);
        }

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> BulkUpdateAsync(List<T> list)
        {
            return await RuYiDbContext.Fastest<T>().BulkUpdateAsync(list);
        }

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> HugeDataBulkCopyAsync(List<T> list)
        {
            var hugeData = RuYiDbContext.Storageable<T>(list).ToStorage();
            return await hugeData.BulkCopyAsync();
        }

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns></returns>
        public async Task<int> HugeDataBulkUpdateAsync(List<T> list)
        {
            var hugeData = RuYiDbContext.Storageable<T>(list).ToStorage();
            return await hugeData.BulkUpdateAsync();
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public async Task<DbResult<bool>> UseTransactionAsync(Func<Task> action, Action<Exception> errorCallBack = null)
        {
            return await RuYiDbContext.AsTenant().UseTranAsync(action, errorCallBack);
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public async Task<DbResult<TEntity>> UseTransactionAsync<TEntity>(Func<Task<TEntity>> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity
        {
            return await RuYiDbContext.AsTenant().UseTranAsync(action, errorCallBack);
        }

        #endregion

        #endregion

        #region 基类私有方法        

        /// <summary>
        /// 获取基本语句
        /// </summary>
        /// <returns></returns>
        private string GetBaseSQL()
        {
            var baseSQL = this.GetSqlByKey("sqls:sql:basequerysql");

            if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.SqlServer)
            {
                baseSQL = baseSQL.Replace("*", "TOP 100 percent *");
            }

            return baseSQL;
        }

        /// <summary>
        /// 获取默认查询语句
        /// </summary>
        /// <returns></returns>
        private string GetDefaultQuerySQL()
        {
            var defaultQuerySQL = this.GetSqlByKey("sqls:sql:defaultquerysql");
            return defaultQuerySQL;
        }

        /// <summary>
        /// 获取实体表名
        /// </summary>
        /// <returns></returns>
        private String GetTableName()
        {
            var tableName = String.Empty;

            T obj = new T();
            var attributes = obj.GetType().GetCustomAttributes(typeof(SugarTable), true);
            if (attributes.Length > 0)
            {
                tableName = ((SugarTable)attributes[0]).TableName;
            }
            else
            {
                tableName = obj.GetType().Name;
            }

            return tableName;
        }

        /// <summary>
        /// 获取查询脚本
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>sql语句</returns>
        private string GetQuerySQL(SearchCondition searchCondition, string sqlKey)
        {
            var sqlStr = this.GetSqlByKey(sqlKey);
            var sql = this.AssembleBasicQueryStatement(searchCondition, sqlStr);
            return sql;
        }

        /// <summary>
        /// 组装基本查询语句
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="strSql">基础SQL语句</param>
        /// <returns>完成SQL语句</returns>
        private String AssembleBasicQueryStatement(SearchCondition searchCondition, String strSql)
        {
            var baseSQL = this.GetBaseSQL();
            baseSQL = string.Format(baseSQL, strSql);

            var condition = this.ConvertQueryCondition(searchCondition);
            var sort = this.ConvertSort(searchCondition.Sort);

            var sql = string.Join("", baseSQL, condition, sort);

            return sql;
        }

        /// <summary>
        /// 转化查询条件
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询语句</returns>
        private string ConvertQueryCondition(SearchCondition searchCondition)
        {
            var sqlStatement = string.Empty;
            if (searchCondition != null && searchCondition.SearchItems != null && searchCondition.SearchItems.Count > 0)
            {
                sqlStatement = searchCondition.ConvertToSqlStatement();
            }
            return sqlStatement;
        }

        /// <summary>
        /// 转化排序条件
        /// </summary>
        /// <param name="sort">排序条件</param>
        /// <returns>排序语句</returns>
        private string ConvertSort(string sort)
        {
            var sortStr = string.Empty;
            if (!string.IsNullOrEmpty(sort))
            {
                sortStr = " ORDER BY " + sort;
            }
            return sortStr;
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="strSQL">查询语句</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询集合</returns>
        private List<T> GetPageList(SearchCondition searchCondition, string strSQL, ref int totalCount)
        {
            var list = new List<T>();
            if (searchCondition.PageIndex >= 0 && searchCondition.PageSize > 0)
            {
                list = RuYiDbContext.SqlQueryable<T>(strSQL).
                ToPageList(searchCondition.PageIndex, searchCondition.PageSize, ref totalCount);
            }
            else
            {
                list = RuYiDbContext.SqlQueryable<T>(strSQL).ToList();
                totalCount = list.Count;
            }
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlStr">查询语句</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        private List<TDTO> GetPageList<TDTO>(SearchCondition searchCondition, string sqlStr, ref int totalCount) where TDTO : class, new()
        {
            var list = new List<TDTO>();
            if (searchCondition.PageIndex >= 0 && searchCondition.PageSize > 0)
            {
                list = RuYiDbContext.SqlQueryable<TDTO>(sqlStr).
                ToPageList(searchCondition.PageIndex, searchCondition.PageSize, ref totalCount);
            }
            else
            {
                list = RuYiDbContext.SqlQueryable<TDTO>(sqlStr).ToList();
                totalCount = list.Count;
            }
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlStr">sql语句</param>
        /// <param name="totalCount">记录条数</param>
        /// <returns>查询结果</returns>
        private async Task<List<T>> GetPageListAsync(SearchCondition searchCondition, string sqlStr, RefAsync<int> totalCount)
        {
            var list = new List<T>();
            if (searchCondition.PageIndex >= 0 && searchCondition.PageSize > 0)
            {
                list = await RuYiDbContext.SqlQueryable<T>(sqlStr).
                ToPageListAsync(searchCondition.PageIndex, searchCondition.PageSize, totalCount);
            }
            else
            {
                list = await RuYiDbContext.SqlQueryable<T>(sqlStr).ToListAsync();
                totalCount = list.Count;
            }
            return list;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlStr">查询语句</param>
        /// <param name="totalCount">记录总数</param>
        /// <returns>查询结果</returns>
        private async Task<List<TDTO>> GetPageListAsync<TDTO>(SearchCondition searchCondition, string sqlStr, RefAsync<int> totalCount) where TDTO : class, new()
        {
            var list = new List<TDTO>();
            if (searchCondition.PageIndex >= 0 && searchCondition.PageSize > 0)
            {
                list = await RuYiDbContext.SqlQueryable<TDTO>(sqlStr).
                ToPageListAsync(searchCondition.PageIndex, searchCondition.PageSize, totalCount);
            }
            else
            {
                list = await RuYiDbContext.SqlQueryable<TDTO>(sqlStr).ToListAsync();
                totalCount = list.Count;
            }
            return list;
        }

        #endregion
    }
}
