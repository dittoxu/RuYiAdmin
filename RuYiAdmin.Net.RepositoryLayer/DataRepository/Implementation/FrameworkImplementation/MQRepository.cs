﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Implementation.FrameworkImplementation
{
    /// <summary>
    /// ActiveMQ访问层实现
    /// </summary>
    public class MQRepository : IMQRepository
    {
        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        public void SendTopic(string message, string topicName = null)
        {
            if (RuYiGlobalConfig.MomConfig.MomType == MomType.ActiveMQ)
            {
                RuYiActiveMQContext.SendTopic(message, topicName);
            }
            else if (RuYiGlobalConfig.MomConfig.MomType == MomType.RabbitMQ)
            {
                RuYiRabbitMQContext.SendMessage(message);
            }
        }

        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        public async Task SendTopicAsync(string message, string topicName = null)
        {
            if (RuYiGlobalConfig.MomConfig.MomType == MomType.ActiveMQ)
            {
                await RuYiActiveMQContext.SendTopicAsync(message, topicName);
            }
            else if (RuYiGlobalConfig.MomConfig.MomType == MomType.RabbitMQ)
            {
                await RuYiRabbitMQContext.SendMessageAsynce(message);
            }
        }

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        public void SendQueue(string message, string queueName = null)
        {
            if (RuYiGlobalConfig.MomConfig.MomType == MomType.ActiveMQ)
            {
                RuYiActiveMQContext.SendQueue(message, queueName);
            }
            else if (RuYiGlobalConfig.MomConfig.MomType == MomType.RabbitMQ)
            {
                RuYiRabbitMQContext.SendMessage(message);
            }
        }

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        public async Task SendQueueAsync(string message, string queueName = null)
        {
            if (RuYiGlobalConfig.MomConfig.MomType == MomType.ActiveMQ)
            {
                await RuYiActiveMQContext.SendQueueAsync(message, queueName);
            }
            else if (RuYiGlobalConfig.MomConfig.MomType == MomType.RabbitMQ)
            {
                await RuYiRabbitMQContext.SendMessageAsynce(message);
            }
        }
    }
}
