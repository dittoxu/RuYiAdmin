﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Utilities.Contexts;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using System;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Implementation.FrameworkImplementation
{
    /// <summary>
    /// 业务日志仓储层实现
    /// </summary>
    public class LoggerRepository : ILoggerRepository
    {
        /// <summary>
        /// 普通日志
        /// </summary>
        /// <param name="message">摘要</param>
        /// <param name="exception">异常</param>
        public void Info(string message, Exception exception = null)
        {
            RuYiLoggerContext.Info(message, exception);
        }

        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="message">摘要</param>
        /// <param name="exception">异常</param>
        public void Warn(string message, Exception exception = null)
        {
            RuYiLoggerContext.Warn(message, exception);
        }

        /// <summary>
        /// 错误日志
        /// </summary>
        /// <param name="message">摘要</param>
        /// <param name="exception">异常</param>
        public void Error(string message, Exception exception = null)
        {
            RuYiLoggerContext.Error(message, exception);
        }

        /// <summary>
        /// Debug日志
        /// </summary>
        /// <param name="message">摘要</param>
        /// <param name="exception">异常</param>
        public void Debug(string message, Exception exception = null)
        {
            RuYiLoggerContext.Debug(message, exception);
        }
    }
}
