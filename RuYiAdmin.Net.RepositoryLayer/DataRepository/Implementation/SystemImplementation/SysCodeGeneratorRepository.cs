﻿using RuYiAdmin.Net.CommonInfrastructure.Classes;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.FrameworkModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Implementation.SystemImplementation
{
    /// <summary>
    /// 代码生成器数据访问层实现
    /// </summary>
    public class SysCodeGeneratorRepository : RuYiAdminDbScope, ISysCodeGeneratorRepository
    {
        #region 获取表名称列表

        /// <summary>
        /// 获取表名称列表
        /// </summary>
        /// <returns>表名称列表</returns>
        public async Task<List<SchemaInfoDTO>> GetSchemaInfo()
        {
            var fields = new List<SchemaInfoDTO>();

            var sqlKey = string.Empty;
            if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.MySql)
            {
                sqlKey = "sqls:sql:query_mysql_schema_info";
            }
            else if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.SqlServer)
            {
                sqlKey = "sqls:sql:query_sqlserver_schema_info";
            }
            else if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.Oracle)
            {
                sqlKey = "sqls:sql:query_oracle_schema_info";
            }
            else if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.PostgreSQL)
            {
                sqlKey = "sqls:sql:query_postgresql_schema_info";
            }
            else
            {
                throw new RuYiAdminCustomException(ExceptionMessage.DatabaseNotSupportedExceptionMessage);
            }

            var strSQL = this.GetSqlByKey(sqlKey);
            fields = (await RuYiDbContext.SqlQueryable<SchemaInfoDTO>(strSQL).ToListAsync()).ToList();

            return fields;
        }

        #endregion

        #region 获取表的列表

        /// <summary>
        /// 获取表的列表
        /// </summary>
        /// <param name="tables">表名</param>
        /// <returns>表的列表集</returns>
        public async Task<List<SchemaInfoDTO>> GetSchemaFieldsInfo(string tables)
        {
            var sqlKey = string.Empty;
            if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.MySql)
            {
                sqlKey = "sqls:sql:query_mysql_schema_column_info";
            }
            else if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.SqlServer)
            {
                sqlKey = "sqls:sql:query_sqlserver_schema_column_info";
            }
            else if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.Oracle)
            {
                sqlKey = "sqls:sql:query_oracle_schema_column_info";
            }
            else if ((DbType)RuYiGlobalConfig.DBConfig.DBType == DbType.PostgreSQL)
            {
                sqlKey = "sqls:sql:query_postgresql_schema_column_info";
            }

            var strSQL = this.GetSqlByKey(sqlKey);
            strSQL = string.Format(strSQL, tables);

            var fields = (await RuYiDbContext.SqlQueryable<SchemaInfoDTO>(strSQL).ToListAsync()).ToList();

            return fields;
        }

        #endregion

        /// <summary>
        /// 通过key获取SQL语句
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>SQL语句</returns>
        public String GetSqlByKey(String key)
        {
            String sqlStr = RuYiGlobalConfig.Configuration.GetSection(key).Value;
            return sqlStr;
        }
    }
}
