﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.BaseRepository.Implementation;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Implementation.SystemImplementation
{
    /// <summary>
    /// 业务附件数据访问层实现
    /// </summary>
    public class SysAttachmentRepository : RuYiAdminBaseRepository<SysAttachment>, ISysAttachmentRepository
    {
        #region 属性及构造函数

        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor context;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="context"></param>
        public SysAttachmentRepository(IHttpContextAccessor context) : base(context)
        {
            this.context = context;
        }

        #endregion
    }
}
