﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Nest;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface
{
    /// <summary>
    /// 
    /// </summary>
    public interface INestRepository
    {
        /// <summary>
        /// 获取ElasticClient实例
        /// </summary>
        /// <returns>ElasticClient</returns>
        ElasticClient GetElasticClient();
    }
}
