﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System.Threading.Tasks;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface
{
    /// <summary>
    /// ActiveMQ访问层接口
    /// </summary>
    public interface IMQRepository
    {
        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        void SendTopic(string message, string topicName = null);

        /// <summary>
        /// 发送Topic
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="topicName">topic别名</param>
        /// <returns></returns>
        Task SendTopicAsync(string message, string topicName = null);

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        void SendQueue(string message, string queueName = null);

        /// <summary>
        /// 发送Queue
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="queueName">queue别名</param>
        /// <returns></returns>
        Task SendQueueAsync(string message, string queueName = null);
    }
}
