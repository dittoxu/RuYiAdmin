﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using MongoDB.Driver;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface
{
    /// <summary>
    /// MongoDB仓储层接口
    /// </summary>
    public interface IMongoDBRepository
    {
        /// <summary>
        /// 获取业务集合
        /// </summary>
        /// <typeparam name="TDocument">TDocument</typeparam>
        /// <param name="collectionName">集合名称</param>
        /// <returns>业务集合</returns>
        IMongoCollection<TDocument> GetBusinessCollection<TDocument>(string collectionName);

        /// <summary>
        /// 获取MongoClient
        /// </summary>
        /// <returns>MongoClient</returns>
        MongoClient GetMongoDBClient();
    }
}
