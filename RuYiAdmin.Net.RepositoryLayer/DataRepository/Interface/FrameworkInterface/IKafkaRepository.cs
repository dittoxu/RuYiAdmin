﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface
{
    /// <summary>
    /// Kafka仓储层接口
    /// </summary>
    public interface IKafkaRepository
    {
        /// <summary>
        /// 发布消息
        /// </summary>
        /// <typeparam name="TMessage">消息类型</typeparam>
        /// <param name="topicName">主题</param>
        /// <param name="message">消息</param>
        /// <returns></returns>
        Task PublishAsync<TMessage>(string topicName, TMessage message) where TMessage : class;

        /// <summary>
        /// 订阅kafka
        /// </summary>
        /// <typeparam name="TMessage">消息类型</typeparam>
        /// <param name="topics">主题</param>
        /// <param name="messageFunc">回调函数</param>
        /// <param name="cancellationToken">取消口令</param>
        /// <returns></returns>
        Task SubscribeAsync<TMessage>(IEnumerable<string> topics, Action<TMessage> messageFunc, CancellationToken cancellationToken) where TMessage : class;
    }
}
