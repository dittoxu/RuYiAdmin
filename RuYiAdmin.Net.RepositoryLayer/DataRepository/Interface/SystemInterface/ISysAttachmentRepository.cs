﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.BaseRepository.Interface;

namespace RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface
{
    /// <summary>
    /// 业务附件数据访问层接口
    /// </summary>
    public interface ISysAttachmentRepository : IRuYiAdminBaseRepository<SysAttachment>
    {
    }
}
