﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 审计日志业务层实现
    /// </summary>
    public class SysLogService : RuYiAdminBaseService<SysLog>, ISysLogService
    {
        /// <summary>
        /// 审计日志仓储实例
        /// </summary>
        private readonly ISysLogRepository LogRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="LogRepository"></param>
        public SysLogService(ISysLogRepository LogRepository) : base(LogRepository)
        {
            this.LogRepository = LogRepository;
        }
    }
}
