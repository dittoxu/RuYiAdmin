﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using AutoMapper;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RuYiAdmin.Net.CommonInfrastructure.Classes;
using RuYiAdmin.Net.CommonInfrastructure.Configurations;
using RuYiAdmin.Net.CommonInfrastructure.Constants.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Constants.System;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.CommonInfrastructure.Exceptions.Framework;
using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.CommonInfrastructure.Utilities.Utils;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Implementation;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessServiceExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.SystemImplementation
{
    /// <summary>
    /// 用户业务层实现
    /// </summary>
    public class SysUserService : RuYiAdminBaseService<SysUser>, ISysUserService
    {
        #region 属性及构造函数

        /// <summary>
        /// 用户仓储实例
        /// </summary>
        private readonly ISysUserRepository UserRepository;

        /// <summary>
        /// 用户与机构仓储实例
        /// </summary>
        private readonly ISysOrgUserRepository OrgUserRepository;

        /// <summary>
        /// AutoMapper实例
        /// </summary>
        private readonly IMapper AutoMapper;

        /// <summary>
        /// Redis仓储实例
        /// </summary>
        private readonly IRedisRepository RedisRepository;

        /// <summary>
        /// HttpContext
        /// </summary>
        private readonly IHttpContextAccessor ContextAccessor;

        /// <summary>
        /// MQ仓储实例
        /// </summary>
        private readonly IMQRepository MQRepository;

        /// <summary>
        /// 业务日志仓储接口实例
        /// </summary>
        private readonly ILoggerRepository LoggerRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="UserRepository"></param>
        /// <param name="OrgUserRepository"></param>
        /// <param name="AutoMapper"></param>
        /// <param name="RedisRepository"></param>
        /// <param name="ContextAccessor"></param>
        /// <param name="MQRepository"></param>
        /// <param name="LoggerRepository"></param>
        public SysUserService(ISysUserRepository UserRepository,
                              ISysOrgUserRepository OrgUserRepository,
                              IMapper AutoMapper,
                              IRedisRepository RedisRepository,
                              IHttpContextAccessor ContextAccessor,
                              IMQRepository MQRepository,
                              ILoggerRepository LoggerRepository) : base(UserRepository)
        {
            this.UserRepository = UserRepository;
            this.OrgUserRepository = OrgUserRepository;
            this.AutoMapper = AutoMapper;
            this.RedisRepository = RedisRepository;
            this.ContextAccessor = ContextAccessor;
            this.MQRepository = MQRepository;
            this.LoggerRepository = LoggerRepository;
        }

        #endregion

        #region 业务层公有方法

        #region 删除系统用户

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userId">用户编号</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> DeleteEntity(Guid userId)
        {
            //删除用户
            await DeleteUser(userId);
            return ActionResponseResult.OK();
        }

        #endregion

        #region 批量删除用户

        /// <summary>
        /// 批量删除用户
        /// </summary>
        /// <param name="ids">编号组</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> DeleteEntities(string ids)
        {
            var array = RuYiStringUtil.GetGuids(ids);

            foreach (var item in array)
            {
                //删除用户
                await DeleteUser(item);
            }

            return ActionResponseResult.OK();
        }

        #endregion

        #region 系统用户登录

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="loginDTO">登录信息</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> Logon(LoginDTO login)
        {
            var prikey = RuYiGlobalConfig.SystemConfig.RsaPrivateKey;

            #region 用户信息解密

            login.UserName = RuYiRsaUtil.PemDecrypt(login.UserName, prikey);
            login.Password = RuYiRsaUtil.PemDecrypt(login.Password, prikey);
            login.CaptchaId = RuYiRsaUtil.PemDecrypt(login.CaptchaId, prikey);
            login.Captcha = RuYiRsaUtil.PemDecrypt(login.Captcha, prikey);

            #endregion

            var obj = new object();

            var resultNum = await this.RedisRepository.GetAsync(login.CaptchaId);

            if (resultNum != null && resultNum.Equals(login.Captcha))
            {
                //删除验证码
                await this.RedisRepository.DeleteAsync(new string[] { login.CaptchaId });

                #region 获取用户信息

                //用户名去盐
                login.UserName = login.UserName.Replace("_" + login.CaptchaId, "");
                //密码去盐
                login.Password = login.Password.Replace("_" + login.CaptchaId, "");

                var users = await this.RedisRepository.GetAsync<List<SysUserDTO>>(RuYiGlobalConfig.SystemCacheConfig.UserCacheName);
                var tempUser = users.Where(t => t.IsDel == (int)DeletionType.Undeleted).Where(t => t.LogonName.Equals(login.UserName)).FirstOrDefault();
                if (tempUser == null)
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.InvalidUserExceptionMessage);
                }

                //AesKey
                var aesKey = RuYiGlobalConfig.SystemConfig.AesKey;
                //AES加密
                login.Password = RuYiAesUtil.Encrypt(login.Password + tempUser.Salt, aesKey);

                var list = users.Where(t => t.IsDel == (int)DeletionType.Undeleted).
                                 Where(t => t.LogonName.Equals(login.UserName)).
                                 Where(t => t.Password.Equals(login.Password)).
                                 Where(t => t.IsEnabled.Equals((int)UserStatus.Enabled)).ToList();

                #endregion

                if (list.Count() == 1)
                {
                    #region 用户状态合法

                    var user = list.FirstOrDefault();

                    var tokenKey = RuYiGlobalConfig.SystemConfig.TokenKey;
                    var tokenExpiration = RuYiGlobalConfig.SystemConfig.UserTokenExpiration;
                    var limitCount = RuYiGlobalConfig.SystemConfig.LogonCountLimit;

                    if (limitCount > 0 && limitCount.Equals(1))
                    {
                        #region 仅允许一次登录

                        var pattern = $"{RuYiGlobalConfig.RedisConfig.Pattern + user.Id + "_"}*";
                        var keys = this.RedisRepository.PatternSearch(pattern);
                        if (keys.Count > 0)
                        {
                            foreach (var item in keys)
                            {
                                //await this.RedisRepository.DeleteAsync(new string[] { item.ToString() });

                                #region 强制用户下线

                                //注销用户
                                await Logout(item, OperationType.ForceLogout, $"{user.DisplayName + "/" + user.LogonName}由于仅允许一次登录被挤下线");

                                var msg = new SystemMessage();
                                msg.Message = Keywords.FORCELOGOUT;
                                msg.MessageType = MessageType.ForceLogout;
                                msg.Object = user;

                                this.MQRepository.SendTopic(JsonConvert.SerializeObject(msg));

                                #endregion
                            }
                        }

                        var token = string.Format(tokenKey, user.Id, Guid.NewGuid());
                        user.Token = token;
                        user.TokenExpiration = tokenExpiration * 60;
                        //登录授权
                        obj = await GetPermissions(user);

                        #endregion
                    }
                    else if (limitCount > 1)
                    {
                        #region 允许多次登录

                        var pattern = $"{RuYiGlobalConfig.RedisConfig.Pattern + user.Id + "_"}*";
                        var keys = this.RedisRepository.PatternSearch(pattern);
                        var count = keys.Count();

                        //达到登录上限
                        if (count.Equals(limitCount))
                        {
                            throw new RuYiAdminCustomException(ExceptionMessage.LogonCountLimitedExceptionMessage);
                        }
                        //未达上限
                        else if (count < limitCount)
                        {
                            var token = string.Format(tokenKey, user.Id, Guid.NewGuid());
                            user.Token = token;
                            user.TokenExpiration = tokenExpiration * 60;
                            //登录授权
                            obj = await GetPermissions(user);
                        }

                        #endregion
                    }

                    #region 记录登录日志

                    var log = new SysLog();

                    log.Id = Guid.NewGuid();
                    log.UserId = user.Id;
                    log.UserName = user.DisplayName + "/" + user.LogonName;

                    log.OrgId = user.OrgId;
                    if (log.OrgId.Equals(Guid.Empty))
                    {
                        log.OrgName = "none";
                    }
                    else
                    {
                        log.OrgName = user.OrgName;
                    }

                    log.System = this.ContextAccessor.HttpContext.Request.Headers["User-Agent"].FirstOrDefault().ToString().Split('(')[1].Split(')')[0];
                    log.Browser = this.ContextAccessor.HttpContext.Request.Headers["sec-ch-ua"];

                    var ip = this.ContextAccessor.HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
                    if (string.IsNullOrEmpty(ip))
                    {
                        ip = this.ContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                    }
                    log.IP = ip;

                    log.OperationType = OperationType.Logon;
                    log.RequestMethod = this.ContextAccessor.HttpContext.Request.Method;
                    log.RequestUrl = "/API/UserManagement/Logon";
                    log.Params = string.Empty;

                    log.Result = string.Empty;
                    log.OldValue = string.Empty;
                    log.NewValue = string.Empty;
                    log.Remark = $"{user.DisplayName}于{DateTime.Now}访问了{log.RequestUrl}接口";

                    log.IsDel = (int)DeletionType.Undeleted;
                    log.Creator = user.Id;
                    log.CreateTime = DateTime.Now;
                    log.Modifier = user.Id;
                    log.ModifyTime = DateTime.Now;
                    log.VersionId = Guid.NewGuid();

                    //记录审计日志
                    await log.WriteAsync();

                    #endregion

                    #endregion
                }
                else
                {
                    throw new RuYiAdminCustomException(ExceptionMessage.InvalidUserExceptionMessage);
                }
            }
            else
            {
                throw new RuYiAdminCustomException(ExceptionMessage.InvalidCaptchaExceptionMessage);
            }

            return ActionResponseResult.OK(obj);
        }

        #endregion

        #region 用户退出登录

        /// <summary>
        /// 退出登录
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="operationType">退出类型</param>
        /// <param name="remark">说明信息</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> Logout(string token, OperationType operationType = OperationType.Logout, string remark = null)
        {
            #region 记录登出日志

            //获取用户
            var user = await this.RedisRepository.GetAsync<SysUserDTO>(token);
            if (user == null)
            {
                throw new RuYiAdminCustomException(ExceptionMessage.InvalidTokenExceptionMessage);
            }

            var log = new SysLog();

            log.Id = Guid.NewGuid();
            log.UserId = user.Id;
            log.UserName = user.DisplayName + "/" + user.LogonName;

            log.OrgId = user.OrgId;
            if (log.OrgId.Equals(Guid.Empty))
            {
                log.OrgName = "none";
            }
            else
            {
                log.OrgName = user.OrgName;
            }

            log.System = this.ContextAccessor.HttpContext.Request.Headers["User-Agent"].FirstOrDefault().ToString().Split('(')[1].Split(')')[0];
            log.Browser = this.ContextAccessor.HttpContext.Request.Headers["sec-ch-ua"];

            var ip = this.ContextAccessor.HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault();
            if (string.IsNullOrEmpty(ip))
            {
                ip = this.ContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            }
            log.IP = ip;

            log.OperationType = operationType;
            log.RequestMethod = this.ContextAccessor.HttpContext.Request.Method;
            log.RequestUrl = "/API/UserManagement/Logout";
            log.Params = string.Empty;

            log.Result = string.Empty;
            log.OldValue = string.Empty;
            log.NewValue = string.Empty;
            log.Remark = string.IsNullOrEmpty(remark) ? $"{user.DisplayName}于{DateTime.Now}访问了{log.RequestUrl}接口" : remark;

            log.IsDel = (int)DeletionType.Undeleted;
            log.Creator = user.Id;
            log.CreateTime = DateTime.Now;
            log.Modifier = user.Id;
            log.ModifyTime = DateTime.Now;
            log.VersionId = Guid.NewGuid();

            //记录审计日志
            await log.WriteAsync();

            #endregion

            if (!string.IsNullOrEmpty(token))
            {
                await this.RedisRepository.DeleteAsync(new string[] { token });
            }

            return ActionResponseResult.OK();
        }

        #endregion

        #region 更新用户密码

        /// <summary>
        /// 更新用户密码
        /// </summary>
        /// <param name="data">参数</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> UpdatePassword(PasswordDTO data)
        {
            var prikey = RuYiGlobalConfig.SystemConfig.RsaPrivateKey;
            var aesKey = RuYiGlobalConfig.SystemConfig.AesKey;

            var userId = RuYiRsaUtil.PemDecrypt(data.UserId, prikey);
            var password = RuYiRsaUtil.PemDecrypt(data.Password, prikey);
            var salt = RuYiRsaUtil.PemDecrypt(data.Salt, prikey);

            //密码去盐
            password = password.Replace("_" + salt, "");

            var user = await this.UserRepository.GetByIdAsync(Guid.Parse(userId));
            user.Salt = Guid.NewGuid();
            user.Password = RuYiAesUtil.Encrypt(password + user.Salt, aesKey);//AES加密
            await this.UserRepository.UpdateEntityAsync(user);

            #region 数据一致性维护

            var users = await this.RedisRepository.GetAsync<List<SysUserDTO>>(RuYiGlobalConfig.SystemCacheConfig.UserCacheName);
            var entity = users.Where(t => t.Id == user.Id).FirstOrDefault();

            if (entity != null)
            {
                users.Remove(entity);//移除旧记录

                entity.Salt = user.Salt;//旧记录赋新值
                entity.Password = user.Password;
                entity.ModifyTime = user.ModifyTime;
                entity.Modifier = user.Modifier;
                entity.VersionId = user.VersionId;

                users.Add(entity);//追加新记录
            }

            await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.UserCacheName, users, -1);

            #endregion

            return ActionResponseResult.OK();
        }

        #endregion

        #region 获取在线用户

        /// <summary>
        /// 获取在线用户
        /// </summary>
        /// <returns>QueryResponseResult</returns>
        public async Task<QueryResponseResult<SysUserDTO>> GetOnlineUsers()
        {
            var list = new List<SysUserDTO>();

            var pattern = $"{RuYiGlobalConfig.RedisConfig.Pattern}*";
            var keys = this.RedisRepository.PatternSearch(pattern);
            foreach (var key in keys)
            {
                list.Add(await this.RedisRepository.GetAsync<SysUserDTO>(key));
            }

            return QueryResponseResult<SysUserDTO>.Success(list.Count, list);
        }

        #endregion

        #region 加载系统用户缓存

        /// <summary>
        /// 加载系统用户缓存
        /// </summary>
        public async Task LoadSystemUserCache()
        {
            var strSQL = this.GetSqlByKey("sqls:sql:query_sysuser");

            int totalCount = 0;
            var users = (await this.UserRepository.SqlQueryAsync<SysUserDTO>(new SearchCondition(), totalCount, strSQL)).ToList();
            await this.RedisRepository.SetAsync(RuYiGlobalConfig.SystemCacheConfig.UserCacheName, users, -1);

            this.LoggerRepository.Info("系统用户缓存加载完成");
        }

        #endregion

        #region 清理系统用户缓存

        /// <summary>
        /// 清理系统用户缓存
        /// </summary>
        public async Task ClearSystemUserCache()
        {
            await this.RedisRepository.DeleteAsync(new string[] { RuYiGlobalConfig.SystemCacheConfig.UserCacheName });

            this.LoggerRepository.Info("系统用户缓存清理完成");
        }

        #endregion

        #endregion

        #region 业务层私有方法

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userId">用户编号</param>
        private async Task DeleteUser(Guid userId)
        {
            //解除用户与机构关系
            var task = await this.OrgUserRepository.GetListAsync();
            var orgUserList = task.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.UserId.Equals(userId)).ToList();

            foreach (var item in orgUserList)
            {
                await this.OrgUserRepository.DeleteEntityAsync(item.Id);
            }

            //删除用户
            await this.UserRepository.DeleteEntityAsync(userId);
        }

        /// <summary>
        /// 用户登录授权
        /// </summary>
        /// <param name="user">用户信息</param>
        /// <returns>Object</returns>
        private async Task<object> GetPermissions(SysUserDTO user)
        {
            var menuList = new List<SysMenuDTO>();

            //超级用户
            if (user.IsSupperAdmin.Equals((int)YesNo.YES) && user.OrgId.Equals(Guid.Empty))
            {
                menuList = await this.RedisRepository.GetAsync<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);
            }
            //普通用户
            else
            {
                var roleUsers = await this.RedisRepository.GetAsync<List<SysRoleUser>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndUserCacheName);
                var roleIds = roleUsers.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.UserId.Equals(user.Id)).Select(t => t.RoleId).ToArray();

                if (roleIds.Length > 0)
                {
                    var roleMenus = await this.RedisRepository.GetAsync<List<SysRoleMenu>>(RuYiGlobalConfig.SystemCacheConfig.RoleAndMenuCacheName);
                    var menus = await this.RedisRepository.GetAsync<List<SysMenuDTO>>(RuYiGlobalConfig.SystemCacheConfig.MenuCacheName);

                    foreach (var roleId in roleIds)
                    {
                        var menuIds = roleMenus.Where(t => t.IsDel == (int)DeletionType.Undeleted && t.RoleId.Equals(roleId)).Select(t => t.MenuId);
                        foreach (var menuId in menuIds)
                        {
                            var menu = menus.Where(t => t.Id == menuId).FirstOrDefault();
                            if (menu != null && menuList.Where(t => t.Id.Equals(menu.Id)).Count().Equals(0))
                            {
                                menuList.Add(menu);
                            }
                        }
                    }
                }
            }

            var permissions = new List<SysMenuDTO>();

            //一级菜单
            var parentNodes = menuList.Where(t => t.MenuType.Equals(MenuType.Menu) && t.ParentId == null).OrderBy(t => t.SerialNumber).ToList();
            foreach (var node in parentNodes)
            {
                var parent = this.AutoMapper.Map<SysMenuDTO>(node);
                parent.Children = new List<SysMenuDTO>();

                //二级菜单
                var menuSons = menuList.Where(t => t.ParentId.Equals(node.Id) && t.MenuType == MenuType.Menu).OrderBy(t => t.SerialNumber).ToList();
                foreach (var subNode in menuSons)
                {
                    //三级菜单：按钮、视图
                    var subItems = menuList.Where(t => t.ParentId.Equals(subNode.Id) && (t.MenuType == MenuType.Button || t.MenuType == MenuType.View)).
                                        OrderBy(t => t.SerialNumber).ToList();

                    if (subItems.Count > 0)
                    {
                        var sonNode = this.AutoMapper.Map<SysMenuDTO>(subNode);
                        sonNode.Children = new List<SysMenuDTO>();

                        var children = this.AutoMapper.Map<List<SysMenuDTO>>(subItems);
                        sonNode.Children.AddRange(children);

                        parent.Children.Add(sonNode);
                    }
                }

                permissions.Add(parent);
            }

            //初始化菜单多语
            await InitLanguages(permissions);
            var codeGeneratorConfig = RuYiGlobalConfig.CodeGeneratorConfig;

            //初始化用户口令
            await this.RedisRepository.SetAsync(user.Token, user, user.TokenExpiration);

            //装填返回结果
            var obj = new { user, permissions, codeGeneratorConfig };

            return obj;
        }

        /// <summary>
        /// 初始化多语
        /// </summary>
        /// <param name="list">菜单列表</param>
        private async Task InitLanguages(List<SysMenuDTO> list)
        {
            if (list.Count > 0)
            {
                var languages = await this.RedisRepository.GetAsync<List<SysLanguage>>(RuYiGlobalConfig.SystemCacheConfig.LanguageCacheName);
                var lanEn = languages.Where(t => t.LanguageName.Equals(Keywords.LANGUAGE_EN)).FirstOrDefault();
                var lanRu = languages.Where(t => t.LanguageName.Equals(Keywords.LANGUAGE_RU)).FirstOrDefault();

                var menuLanguages = await this.RedisRepository.GetAsync<List<SysMenuLanguage>>(RuYiGlobalConfig.SystemCacheConfig.MenuAndLanguageCacheName);

                foreach (var item in list)
                {
                    var enMenu = menuLanguages.Where(t => t.MenuId.Equals(item.Id) && t.LanguageId.Equals(lanEn.Id) && t.IsDel == (int)DeletionType.Undeleted).FirstOrDefault();
                    var ruMenu = menuLanguages.Where(t => t.MenuId.Equals(item.Id) && t.LanguageId.Equals(lanRu.Id) && t.IsDel == (int)DeletionType.Undeleted).FirstOrDefault();

                    if (enMenu != null)
                    {
                        item.MenuNameEn = enMenu.MenuName;
                    }

                    if (ruMenu != null)
                    {
                        item.MenuNameRu = ruMenu.MenuName;
                    }

                    if (item.Children != null && item.Children.Count > 0)
                    {
                        await InitLanguages(item.Children);
                    }
                }
            }
        }

        #endregion
    }
}
