﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using System;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.FrameworkImplementation
{
    /// <summary>
    /// 业务日志服务层实现
    /// </summary>
    public class LoggerService : ILoggerService
    {
        #region 属性及其构造函数

        /// <summary>
        /// 日志仓储实例
        /// </summary>
        private readonly ILoggerRepository LoggerRepository;

        public LoggerService(ILoggerRepository LoggerRepository)
        {
            this.LoggerRepository = LoggerRepository;
        }

        #endregion

        #region 日志公有方法

        public void Info(string message, Exception exception = null)
        {
            this.LoggerRepository.Info(message, exception);
        }

        public void Warn(string message, Exception exception = null)
        {
            this.LoggerRepository.Warn(message, exception);
        }

        public void Error(string message, Exception exception = null)
        {
            this.LoggerRepository.Error(message, exception);
        }

        public void Debug(string message, Exception exception = null)
        {
            this.LoggerRepository.Debug(message, exception);
        }

        #endregion
    }
}
