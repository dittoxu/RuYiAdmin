﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using MongoDB.Driver;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.FrameworkImplementation
{
    /// <summary>
    /// MongoDB服务层实现
    /// </summary>
    public class MongoDBService : IMongoDBService
    {
        /// <summary>
        /// MongoDB仓储层实例
        /// </summary>
        private readonly IMongoDBRepository MongoDBRepository;

        public MongoDBService(IMongoDBRepository MongoDBRepository)
        {
            this.MongoDBRepository = MongoDBRepository;
        }

        /// <summary>
        /// 获取业务集合
        /// </summary>
        /// <typeparam name="TDocument">TDocument</typeparam>
        /// <param name="collectionName">集合名称</param>
        /// <returns>业务集合</returns>
        public IMongoCollection<TDocument> GetBusinessCollection<TDocument>(string collectionName)
        {
            return this.MongoDBRepository.GetBusinessCollection<TDocument>(collectionName);
        }

        /// <summary>
        /// 获取MongoClient
        /// </summary>
        /// <returns>MongoClient</returns>
        public MongoClient GetMongoDBClient()
        {
            return this.MongoDBRepository.GetMongoDBClient();
        }
    }
}
