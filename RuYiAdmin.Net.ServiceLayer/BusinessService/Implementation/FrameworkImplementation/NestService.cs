﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Nest;
using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.FrameworkImplementation
{
    /// <summary>
    /// Nest服务层实现
    /// </summary>
    public class NestService : INestService
    {
        /// <summary>
        /// Nest仓储层实例
        /// </summary>
        private readonly INestRepository NestRepository;

        public NestService(INestRepository NestRepository)
        {
            this.NestRepository = NestRepository;
        }

        /// <summary>
        /// 获取ElasticClient实例
        /// </summary>
        /// <returns>ElasticClient</returns>
        public ElasticClient GetElasticClient()
        {
            return this.NestRepository.GetElasticClient();
        }
    }
}
