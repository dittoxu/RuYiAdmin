﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.RepositoryLayer.DataRepository.Interface.FrameworkInterface;
using RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Implementation.FrameworkImplementation
{
    /// <summary>
    /// Kafka服务层实现
    /// </summary>
    public class KafkaService : IKafkaService
    {
        private readonly IKafkaRepository KafkaRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="KafkaRepository"></param>
        public KafkaService(IKafkaRepository KafkaRepository)
        {
            this.KafkaRepository = KafkaRepository;
        }

        /// <summary>
        /// 发布消息
        /// </summary>
        /// <typeparam name="TMessage">消息类型</typeparam>
        /// <param name="topicName">主题</param>
        /// <param name="message">消息</param>
        /// <returns></returns>
        public async Task PublishAsync<TMessage>(string topicName, TMessage message) where TMessage : class
        {
            await this.KafkaRepository.PublishAsync(topicName, message);
        }

        /// <summary>
        /// 订阅kafka
        /// </summary>
        /// <typeparam name="TMessage">消息类型</typeparam>
        /// <param name="topics">主题</param>
        /// <param name="messageFunc">回调函数</param>
        /// <param name="cancellationToken">取消口令</param>
        /// <returns></returns>
        public async Task SubscribeAsync<TMessage>(IEnumerable<string> topics, Action<TMessage> messageFunc, CancellationToken cancellationToken) where TMessage : class
        {
            await this.KafkaRepository.SubscribeAsync(topics, messageFunc, cancellationToken);
        }
    }
}
