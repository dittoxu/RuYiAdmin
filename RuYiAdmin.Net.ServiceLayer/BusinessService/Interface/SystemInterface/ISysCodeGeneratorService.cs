﻿using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.FrameworkModel;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 代码生产服务层接口
    /// </summary>
    public interface ISysCodeGeneratorService
    {
        /// <summary>
        /// 获取表名称列表
        /// </summary>
        /// <returns>表名称列表</returns>
        Task<List<SchemaInfoDTO>> GetSchemaInfo();

        /// <summary>
        /// 自动生成代码
        /// </summary>
        /// <param name="codeGenerator">参数</param>
        /// <returns>zipId</returns>
        Task<Guid> CodeGenerate(CodeGeneratorDTO codeGenerator);
    }
}
