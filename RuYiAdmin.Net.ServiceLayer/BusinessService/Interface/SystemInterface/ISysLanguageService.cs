﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 多语业务层接口
    /// </summary>
    public interface ISysLanguageService : IRuYiAdminBaseService<SysLanguage>
    {
        /// <summary>
        /// 加载系统多语缓存
        /// </summary>
        Task LoadSystemLanguageCache();

        /// <summary>
        /// 清理系统多语缓存
        /// </summary>
        Task ClearSystemLanguageCache();
    }
}
