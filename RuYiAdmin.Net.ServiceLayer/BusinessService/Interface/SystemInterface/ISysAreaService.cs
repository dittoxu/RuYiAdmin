﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.SystemInterface
{
    /// <summary>
    /// 行政区域服务层接口
    /// </summary>
    public interface ISysAreaService : IRuYiAdminBaseService<SysArea>
    {
        /// <summary>
        /// 加载行政区域缓存
        /// </summary>
        Task LoadSysAreaCache();

        /// <summary>
        /// 清理行政区域缓存
        /// </summary>
        Task ClearSysAreaCache();

        /// <summary>
        /// 获取行政区域树
        /// </summary>
        /// <returns></returns>
        Task<QueryResponseResult<SysAreaDTO>> GetAreaTreeNodes();
    }
}
