﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2023 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Nest;

namespace RuYiAdmin.Net.ServiceLayer.BusinessService.Interface.FrameworkInterface
{
    /// <summary>
    /// Nest服务层接口
    /// </summary>
    public interface INestService
    {
        /// <summary>
        /// 获取ElasticClient实例
        /// </summary>
        /// <returns>ElasticClient</returns>
        ElasticClient GetElasticClient();
    }
}
