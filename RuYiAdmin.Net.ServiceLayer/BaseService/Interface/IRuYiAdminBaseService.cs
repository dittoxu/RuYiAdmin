﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BaseService.Interface
{
    /// <summary>
    /// 服务接口
    /// </summary>
    public interface IRuYiAdminBaseService<T> where T : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        QueryResponseResult<T> GetPage(SearchCondition searchCondition);

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        QueryResponseResult<T> GetList(SearchCondition searchCondition);

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        ActionResponseResult GetById(Guid id);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>查询结果</returns>
        QueryResponseResult<T> SqlQuery(SearchCondition searchCondition, string sqlKey);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="strSQL">sql语句</param>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        QueryResponseResult<T> SqlQuery(string strSQL, SearchCondition searchCondition);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>查询结果</returns>
        QueryResponseResult<TDTO> SqlQuery<TDTO>(SearchCondition searchCondition, string sqlKey) where TDTO : class, new();

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="strSQL">查询语句</param>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        QueryResponseResult<TDTO> SqlQuery<TDTO>(string strSQL, SearchCondition searchCondition) where TDTO : class, new();

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        QueryResponseResult<T> DefaultSqlQuery(SearchCondition searchCondition);

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        QueryResponseResult<TDTO> DefaultSqlQuery<TDTO>(SearchCondition searchCondition) where TDTO : class, new();

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>执行结果</returns>
        ActionResponseResult GetList();

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <returns>查询结果</returns>
        QueryResponseResult<TDTO> GetList<TDTO>() where TDTO : class, new();

        /// <summary>
        /// 按表达式查询
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns>执行结果</returns>
        ActionResponseResult QueryByExpression(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        ActionResponseResult Add(T obj, bool create = true, bool transactional = true);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        ActionResponseResult AddList(List<T> list, bool create = true, bool transactional = true);

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        ActionResponseResult Update(T obj, bool transactional = true);

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        ActionResponseResult UpdateList(List<T> list, bool transactional = true);

        /// <summary>
        /// 逻辑删除对象
        /// </summary>
        /// <param name="id">对象编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        ActionResponseResult Delete(Guid id, bool transactional = true);

        /// <summary>
        /// 批量逻辑删除对象
        /// </summary>
        /// <param name="ids">对象编号数组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        ActionResponseResult DeleteRange(Guid[] ids, bool transactional = true);

        /// <summary>
        /// 物理删除对象
        /// </summary>
        /// <param name="id">对象编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        ActionResponseResult Remove(Guid id, bool transactional = true);

        /// <summary>
        /// 批量物理删除对象
        /// </summary>
        /// <param name="ids">对象编号数组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        ActionResponseResult RemoveRange(Guid[] ids, bool transactional = true);

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        ActionResponseResult BulkCopy(List<T> list);

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        ActionResponseResult PageBulkCopy(int pageSize, List<T> list);

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        ActionResponseResult BulkUpdate(List<T> list);

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        ActionResponseResult HugeDataBulkCopy(List<T> list);

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        ActionResponseResult HugeDataBulkUpdate(List<T> list);

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        DbResult<bool> UseTransaction(Action action, Action<Exception> errorCallBack = null);

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        DbResult<TEntity> UseTransaction<TEntity>(Func<TEntity> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity;

        /// <summary>
        /// 通过key获取SQL语句
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>SQL语句</returns>
        String GetSqlByKey(String key);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<T>> GetPageAsync(SearchCondition searchCondition);

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<T>> GetListAsync(SearchCondition searchCondition);

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        Task<ActionResponseResult> GetByIdAsync(Guid id);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<T>> SqlQueryAsync(SearchCondition searchCondition, string sqlKey);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="strSQL">sql语句</param>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<T>> SqlQueryAsync(string strSQL, SearchCondition searchCondition);

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<TDTO>> SqlQueryAsync<TDTO>(SearchCondition searchCondition, string sqlKey) where TDTO : class, new();

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="strSQL">sql语句</param>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<TDTO>> SqlQueryAsync<TDTO>(string strSQL, SearchCondition searchCondition) where TDTO : class, new();

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<T>> DefaultSqlQueryAsync(SearchCondition searchCondition);

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<TDTO>> DefaultSqlQueryAsync<TDTO>(SearchCondition searchCondition) where TDTO : class, new();

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>列表</returns>
        Task<ActionResponseResult> GetListAsync();

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <returns>查询结果</returns>
        Task<QueryResponseResult<TDTO>> GetListAsync<TDTO>() where TDTO : class, new();

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        Task<ActionResponseResult> AddAsync(T obj, bool create = true, bool transactional = true);

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        Task<ActionResponseResult> AddListAsync(List<T> list, bool create = true, bool transactional = true);

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        Task<ActionResponseResult> UpdateAsync(T obj, bool transactional = true);

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        Task<ActionResponseResult> UpdateListAsync(List<T> list, bool transactional = true);

        /// <summary>
        /// 逻辑删除对象
        /// </summary>
        /// <param name="id">对象编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        Task<ActionResponseResult> DeleteAsync(Guid id, bool transactional = true);

        /// <summary>
        /// 批量逻辑删除对象
        /// </summary>
        /// <param name="ids">对象编号数组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        Task<ActionResponseResult> DeleteRangeAsync(Guid[] ids, bool transactional = true);

        /// <summary>
        /// 物理删除对象
        /// </summary>
        /// <param name="id">对象编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        Task<ActionResponseResult> RemoveAsync(Guid id, bool transactional = true);

        /// <summary>
        /// 批量物理删除对象
        /// </summary>
        /// <param name="ids">对象编号数组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        Task<ActionResponseResult> RemoveRangeAsync(Guid[] ids, bool transactional = true);

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> BulkCopyAsync(List<T> list);

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> PageBulkCopyAsync(int pageSize, List<T> list);

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> BulkUpdateAsync(List<T> list);

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> HugeDataBulkCopyAsync(List<T> list);

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        Task<ActionResponseResult> HugeDataBulkUpdateAsync(List<T> list);

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        Task<DbResult<bool>> UseTransactionAsync(Func<Task> action, Action<Exception> errorCallBack = null);

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        Task<DbResult<TEntity>> UseTransactionAsync<TEntity>(Func<Task<TEntity>> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity;
    }
}
