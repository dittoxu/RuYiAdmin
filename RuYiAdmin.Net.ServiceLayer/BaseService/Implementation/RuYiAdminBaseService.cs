﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Models;
using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using RuYiAdmin.Net.RepositoryLayer.BaseRepository.Interface;
using RuYiAdmin.Net.ServiceLayer.BaseService.Interface;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;

namespace RuYiAdmin.Net.ServiceLayer.BaseService.Implementation
{
    /// <summary>
    /// 服务基类
    /// </summary>
    public class RuYiAdminBaseService<T> : IRuYiAdminBaseService<T> where T : RuYiAdminBaseEntity
    {
        #region 属性及构造函数

        /// <summary>
        /// 基类仓储实例
        /// </summary>
        private readonly IRuYiAdminBaseRepository<T> RuYiAdminBaseRepository;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="RuYiAdminBaseRepository"></param>
        public RuYiAdminBaseService(IRuYiAdminBaseRepository<T> RuYiAdminBaseRepository)
        {
            this.RuYiAdminBaseRepository = RuYiAdminBaseRepository;
        }

        #endregion

        #region 同步方法

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public QueryResponseResult<T> GetPage(SearchCondition searchCondition)
        {
            var totalCount = 0;

            var queryResponseResult = new QueryResponseResult<T>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = this.RuYiAdminBaseRepository.GetPage(searchCondition, ref totalCount);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public QueryResponseResult<T> GetList(SearchCondition searchCondition)
        {
            var queryResponseResult = new QueryResponseResult<T>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = this.RuYiAdminBaseRepository.GetList(searchCondition);
            queryResponseResult.TotalCount = queryResponseResult.List.Count;

            return queryResponseResult;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        public ActionResponseResult GetById(Guid id)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.GetById(id);

            return actionResponseResult;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>查询结果</returns>
        public QueryResponseResult<T> SqlQuery(SearchCondition searchCondition, string sqlKey)
        {
            var totalCount = 0;

            var queryResponseResult = new QueryResponseResult<T>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = this.RuYiAdminBaseRepository.SqlQuery(searchCondition, sqlKey, ref totalCount);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="strSQL">sql语句</param>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public QueryResponseResult<T> SqlQuery(string strSQL, SearchCondition searchCondition)
        {
            var totalCount = 0;

            var queryResponseResult = new QueryResponseResult<T>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = this.RuYiAdminBaseRepository.SqlQuery(searchCondition, ref totalCount, strSQL);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>查询结果</returns>
        public QueryResponseResult<TDTO> SqlQuery<TDTO>(SearchCondition searchCondition, string sqlKey) where TDTO : class, new()
        {
            var totalCount = 0;

            var queryResponseResult = new QueryResponseResult<TDTO>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = this.RuYiAdminBaseRepository.SqlQuery<TDTO>(searchCondition, sqlKey, ref totalCount);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="strSQL">查询语句</param>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public QueryResponseResult<TDTO> SqlQuery<TDTO>(string strSQL, SearchCondition searchCondition) where TDTO : class, new()
        {
            var totalCount = 0;

            var queryResponseResult = new QueryResponseResult<TDTO>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = this.RuYiAdminBaseRepository.SqlQuery<TDTO>(searchCondition, ref totalCount, strSQL);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public QueryResponseResult<T> DefaultSqlQuery(SearchCondition searchCondition)
        {
            var totalCount = 0;
            List<T> list = this.RuYiAdminBaseRepository.DefaultSqlQuery(searchCondition, ref totalCount);
            return QueryResponseResult<T>.Success(totalCount, list);
        }

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public QueryResponseResult<TDTO> DefaultSqlQuery<TDTO>(SearchCondition searchCondition) where TDTO : class, new()
        {
            var totalCount = 0;
            List<TDTO> list = this.RuYiAdminBaseRepository.DefaultSqlQuery<TDTO>(searchCondition, ref totalCount);
            return QueryResponseResult<TDTO>.Success(totalCount, list);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>执行结果</returns>
        public ActionResponseResult GetList()
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.GetList();

            return actionResponseResult;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <returns>查询结果</returns>
        public QueryResponseResult<TDTO> GetList<TDTO>() where TDTO : class, new()
        {
            var queryResponseResult = new QueryResponseResult<TDTO>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            var list = this.RuYiAdminBaseRepository.GetList<TDTO>();
            queryResponseResult.List = list;
            queryResponseResult.TotalCount = list.Count;

            return queryResponseResult;
        }

        /// <summary>
        /// 按表达式查询
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult QueryByExpression(Expression<Func<T, bool>> expression)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.QueryByExpression(expression);

            return actionResponseResult;
        }

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult Add(T obj, bool create = true, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.AddEntity(obj, create, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult AddList(List<T> list, bool create = true, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.AddList(list, create, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult Update(T obj, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.UpdateEntity(obj, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult UpdateList(List<T> list, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.UpdateList(list, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 逻辑删除对象
        /// </summary>
        /// <param name="id">对象编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult Delete(Guid id, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.DeleteEntity(id, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 批量逻辑删除对象
        /// </summary>
        /// <param name="ids">对象编号数组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult DeleteRange(Guid[] ids, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.DeleteRange(ids, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 物理删除对象
        /// </summary>
        /// <param name="id">对象编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult Remove(Guid id, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.RemoveEntity(id, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 批量物理删除对象
        /// </summary>
        /// <param name="ids">对象编号数组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public ActionResponseResult RemoveRange(Guid[] ids, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.RemoveRange(ids, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public ActionResponseResult BulkCopy(List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.BulkCopy(list);

            return actionResponseResult;
        }

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public ActionResponseResult PageBulkCopy(int pageSize, List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.PageBulkCopy(pageSize, list);

            return actionResponseResult;
        }

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public ActionResponseResult BulkUpdate(List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.BulkUpdate(list);

            return actionResponseResult;
        }

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public ActionResponseResult HugeDataBulkCopy(List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.HugeDataBulkCopy(list);

            return actionResponseResult;
        }

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public ActionResponseResult HugeDataBulkUpdate(List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = this.RuYiAdminBaseRepository.HugeDataBulkUpdate(list);

            return actionResponseResult;
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public DbResult<bool> UseTransaction(Action action, Action<Exception> errorCallBack = null)
        {
            return this.RuYiAdminBaseRepository.UseTransaction(action, errorCallBack);
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public DbResult<TEntity> UseTransaction<TEntity>(Func<TEntity> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity
        {
            return this.RuYiAdminBaseRepository.UseTransaction(action, errorCallBack);
        }

        /// <summary>
        /// 通过key获取SQL语句
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>SQL语句</returns>
        public String GetSqlByKey(String key)
        {
            return this.RuYiAdminBaseRepository.GetSqlByKey(key);
        }

        #endregion

        #region 异步方法

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<T>> GetPageAsync(SearchCondition searchCondition)
        {
            RefAsync<int> totalCount = 0;

            var queryResponseResult = new QueryResponseResult<T>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = await this.RuYiAdminBaseRepository.GetPageAsync(searchCondition, totalCount);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<T>> GetListAsync(SearchCondition searchCondition)
        {
            var queryResponseResult = new QueryResponseResult<T>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = await this.RuYiAdminBaseRepository.GetListAsync(searchCondition);
            queryResponseResult.TotalCount = queryResponseResult.List.Count;

            return queryResponseResult;
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="id">编号</param>
        /// <returns>实体</returns>
        public async Task<ActionResponseResult> GetByIdAsync(Guid id)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.GetByIdAsync(id);

            return actionResponseResult;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<T>> SqlQueryAsync(SearchCondition searchCondition, string sqlKey)
        {
            RefAsync<int> totalCount = 0;

            var queryResponseResult = new QueryResponseResult<T>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = await this.RuYiAdminBaseRepository.SqlQueryAsync(searchCondition, sqlKey, totalCount);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <param name="strSQL">sql语句</param>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<T>> SqlQueryAsync(string strSQL, SearchCondition searchCondition)
        {
            RefAsync<int> totalCount = 0;

            var queryResponseResult = new QueryResponseResult<T>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = await this.RuYiAdminBaseRepository.SqlQueryAsync(searchCondition, totalCount, strSQL);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }
        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <param name="sqlKey">sql键值</param>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<TDTO>> SqlQueryAsync<TDTO>(SearchCondition searchCondition, string sqlKey) where TDTO : class, new()
        {
            RefAsync<int> totalCount = 0;

            var queryResponseResult = new QueryResponseResult<TDTO>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = await this.RuYiAdminBaseRepository.SqlQueryAsync<TDTO>(searchCondition, sqlKey, totalCount);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// SQL查询
        /// </summary>
        /// <typeparam name="TDTO">DTO</typeparam>
        /// <param name="strSQL">sql语句</param>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<TDTO>> SqlQueryAsync<TDTO>(string strSQL, SearchCondition searchCondition) where TDTO : class, new()
        {
            RefAsync<int> totalCount = 0;

            var queryResponseResult = new QueryResponseResult<TDTO>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            queryResponseResult.List = await this.RuYiAdminBaseRepository.SqlQueryAsync<TDTO>(searchCondition, totalCount, strSQL);
            queryResponseResult.TotalCount = totalCount;

            return queryResponseResult;
        }

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<T>> DefaultSqlQueryAsync(SearchCondition searchCondition)
        {
            RefAsync<int> totalCount = 0;
            List<T> list = await this.RuYiAdminBaseRepository.DefaultSqlQueryAsync(searchCondition, totalCount);
            return QueryResponseResult<T>.Success(totalCount, list);
        }

        /// <summary>
        /// 默认SQL查询
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <param name="searchCondition">查询条件</param>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<TDTO>> DefaultSqlQueryAsync<TDTO>(SearchCondition searchCondition) where TDTO : class, new()
        {
            RefAsync<int> totalCount = 0;
            List<TDTO> list = await this.RuYiAdminBaseRepository.DefaultSqlQueryAsync<TDTO>(searchCondition, totalCount);
            return QueryResponseResult<TDTO>.Success(totalCount, list);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns>列表</returns>
        public async Task<ActionResponseResult> GetListAsync()
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.GetListAsync();

            return actionResponseResult;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <typeparam name="TDTO">TDTO</typeparam>
        /// <returns>查询结果</returns>
        public async Task<QueryResponseResult<TDTO>> GetListAsync<TDTO>() where TDTO : class, new()
        {
            var queryResponseResult = new QueryResponseResult<TDTO>();

            queryResponseResult.HttpStatusCode = HttpStatusCode.OK;
            queryResponseResult.Message = new string("OK");
            var list = await this.RuYiAdminBaseRepository.GetListAsync<TDTO>();
            queryResponseResult.List = list;
            queryResponseResult.TotalCount = list.Count;

            return queryResponseResult;
        }

        /// <summary>
        /// 新增对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public async Task<ActionResponseResult> AddAsync(T obj, bool create = true, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.AddEntityAsync(obj, create, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="create">是否赋值</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public async Task<ActionResponseResult> AddListAsync(List<T> list, bool create = true, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.AddListAsync(list, create, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 编辑对象
        /// </summary>
        /// <param name="obj">对象</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public async Task<ActionResponseResult> UpdateAsync(T obj, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.UpdateEntityAsync(obj, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public async Task<ActionResponseResult> UpdateListAsync(List<T> list, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.UpdateListAsync(list, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 逻辑删除对象
        /// </summary>
        /// <param name="id">对象编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public async Task<ActionResponseResult> DeleteAsync(Guid id, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.DeleteEntityAsync(id, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 批量逻辑删除对象
        /// </summary>
        /// <param name="ids">对象编号数组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public async Task<ActionResponseResult> DeleteRangeAsync(Guid[] ids, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.DeleteRangeAsync(ids, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 物理删除对象
        /// </summary>
        /// <param name="id">对象编号</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public async Task<ActionResponseResult> RemoveAsync(Guid id, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.RemoveEntityAsync(id, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 批量物理删除对象
        /// </summary>
        /// <param name="ids">对象编号数组</param>
        /// <param name="transactional">是否开启事务</param>
        /// <returns>执行结果</returns>
        public async Task<ActionResponseResult> RemoveRangeAsync(Guid[] ids, bool transactional = true)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.RemoveRangeAsync(ids, transactional);

            return actionResponseResult;
        }

        /// <summary>
        /// 大数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> BulkCopyAsync(List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.BulkCopyAsync(list);

            return actionResponseResult;
        }

        /// <summary>
        /// 大数据分页写入
        /// </summary>
        /// <param name="pageSize">页数</param>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> PageBulkCopyAsync(int pageSize, List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.PageBulkCopyAsync(pageSize, list);

            return actionResponseResult;
        }

        /// <summary>
        /// 大数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> BulkUpdateAsync(List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.BulkUpdateAsync(list);

            return actionResponseResult;
        }

        /// <summary>
        /// 海量数据写入
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> HugeDataBulkCopyAsync(List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.HugeDataBulkCopyAsync(list);

            return actionResponseResult;
        }

        /// <summary>
        /// 海量数据更新
        /// </summary>
        /// <param name="list">集合</param>
        /// <returns>ActionResponseResult</returns>
        public async Task<ActionResponseResult> HugeDataBulkUpdateAsync(List<T> list)
        {
            var actionResponseResult = new ActionResponseResult();

            actionResponseResult.HttpStatusCode = HttpStatusCode.OK;
            actionResponseResult.Message = new string("OK");
            actionResponseResult.Object = await this.RuYiAdminBaseRepository.HugeDataBulkUpdateAsync(list);

            return actionResponseResult;
        }


        /// <summary>
        /// 使用事务
        /// </summary>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public async Task<DbResult<bool>> UseTransactionAsync(Func<Task> action, Action<Exception> errorCallBack = null)
        {
            return await this.RuYiAdminBaseRepository.UseTransactionAsync(action, errorCallBack);
        }

        /// <summary>
        /// 使用事务
        /// </summary>
        /// <typeparam name="TEntity">数据类型</typeparam>
        /// <param name="action">委托事件</param>
        /// <param name="errorCallBack">错误回调事件</param>
        /// <returns>DbResult</returns>
        public async Task<DbResult<TEntity>> UseTransactionAsync<TEntity>(Func<Task<TEntity>> action, Action<Exception> errorCallBack = null) where TEntity : RuYiAdminBaseEntity
        {
            return await this.RuYiAdminBaseRepository.UseTransactionAsync(action, errorCallBack);
        }

        #endregion
    }
}

