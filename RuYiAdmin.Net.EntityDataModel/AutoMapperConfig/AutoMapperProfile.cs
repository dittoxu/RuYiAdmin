﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using AutoMapper;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.BusinessModel.ModuleManagement;
using RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel;
using RuYiAdmin.Net.EntityDataModel.EntityModel.BusinessModel.ModuleManagement;
using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;

namespace RuYiAdmin.Net.EntityDataModel.AutoMapperConfig
{
    /// <summary>
    /// 映射描述文件
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// AutoMapper映射描述
        /// </summary>
        public AutoMapperProfile()
        {
            #region POCO TO DTO

            //机构
            CreateMap<SysOrganization, SysOrganizationDTO>();

            //用户
            CreateMap<SysUser, SysUserDTO>();

            //菜单
            CreateMap<SysMenu, SysMenuDTO>();

            //角色
            CreateMap<SysRole, SysRoleDTO>();

            //数据字典
            CreateMap<SysCodeTable, SysCodeTableDTO>();

            //导入配置
            CreateMap<SysImportConfig, ImportConfigDTO>();

            //导入配置明细
            CreateMap<SysImportConfigDetail, ImportConfigDetailDTO>();

            //同步账号模型
            CreateMap<BizAccount, BizAccountDTO>();

            #endregion

            #region DTO TO POCO



            #endregion
        }
    }
}
