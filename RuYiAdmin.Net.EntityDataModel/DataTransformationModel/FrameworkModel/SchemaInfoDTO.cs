﻿using RuYiAdmin.Net.EntityDataModel.EntityModel.FrameworkModel;
using SqlSugar;
using System;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.FrameworkModel
{
    /// <summary>
    /// 数据库模型信息
    /// </summary>
    public class SchemaInfoDTO : SchemaField
    {
        /// <summary>
        /// 数据库名称
        /// </summary>
        [SugarColumn(ColumnName = "TABLE_SCHEMA")]
        public string SchemaName { get; set; }

        /// <summary>
        /// 表名称
        /// </summary>
        [SugarColumn(ColumnName = "TABLE_NAME")]
        public string TableName { get; set; }

        /// <summary>
        /// 表注释
        /// </summary>
        [SugarColumn(ColumnName = "TABLE_COMMENT")]
        public string TableComment { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [SugarColumn(ColumnName = "CREATE_TIME")]
        public DateTime CreateTime { get; set; }
    }
}
