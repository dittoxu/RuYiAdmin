﻿using RuYiAdmin.Net.CommonInfrastructure.Configurations;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel
{
    /// <summary>
    /// 代码生成器DTO
    /// </summary>
    public class CodeGeneratorDTO
    {
        /// <summary>
        /// 模型层命名空间
        /// </summary>
        public string EntityNamespace { get; set; }

        /// <summary>
        /// DTO模型命名空间
        /// </summary>
        public string DTONamespace { get; set; }

        /// <summary>
        /// 控制层命名空间
        /// </summary>
        public string ControllerNamespace { get; set; }

        /// <summary>
        /// 服务层命名空间
        /// </summary>
        public string ServiceNamespace { get; set; }

        /// <summary>
        /// 仓储层命名空间
        /// </summary>
        public string RepositoryNamespace { get; set; }

        /// <summary>
        /// 表数组
        /// </summary>
        public string Tables { get; set; }

        /// <summary>
        /// 布局方式
        /// 0:上下布局
        /// 1:左右布局
        /// </summary>
        public int LayoutMode { get; set; }

        /// <summary>
        /// 自动补全路径
        /// </summary>
        public void AutoFillFullName()
        {
            DTONamespace = RuYiGlobalConfig.CodeGeneratorConfig.DTONamespace + EntityNamespace;
            EntityNamespace = RuYiGlobalConfig.CodeGeneratorConfig.EntityNamespace + EntityNamespace;
            ControllerNamespace = RuYiGlobalConfig.CodeGeneratorConfig.ControllerNamespace + ControllerNamespace;
            ServiceNamespace = RuYiGlobalConfig.CodeGeneratorConfig.ServiceNamespace + ServiceNamespace;
            RepositoryNamespace = RuYiGlobalConfig.CodeGeneratorConfig.RepositoryNamespace + RepositoryNamespace;
        }
    }
}
