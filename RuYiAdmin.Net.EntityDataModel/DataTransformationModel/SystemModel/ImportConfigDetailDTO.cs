﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel
{
    /// <summary>
    /// 导入配置明细DTO
    /// </summary>
    public class ImportConfigDetailDTO
    {
        /// <summary>
        /// 数据类型
        /// </summary>
        public CellDataType DataType { get; set; }

        /// <summary>
        /// 所在列
        /// </summary>
        public string Cells { get; set; }

        /// <summary>
        /// 是否必填项
        /// 0：否
        /// 1：是
        /// </summary>
        public int? Rquired { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public double? MaxValue { get; set; }

        /// <summary>
        /// 最小值
        /// </summary>
        public double? MinValue { get; set; }

        /// <summary>
        /// 小数位上限
        /// </summary>
        public int? DecimalLimit { get; set; }

        /// <summary>
        /// 枚举列表
        /// </summary>
        public string TextEnum { get; set; }

        /// <summary>
        /// 扩展字段
        /// 正则占用
        /// </summary>
        public string Extend1 { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public string Extend2 { get; set; }

        /// <summary>
        /// 扩展字段
        /// </summary>
        public string Extend3 { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int? SerialNumber { get; set; }
    }
}
