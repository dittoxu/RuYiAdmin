﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using System;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel
{
    public class QuartzJobDTO
    {
        /// <summary>
        /// 任务编号
        /// </summary>
        public Guid JobId { get; set; }

        /// <summary>
        /// 集群节点编号
        /// </summary>
        public int? GroupId { get; set; }

        /// <summary>
        /// 预执行动作
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// 操作人
        /// </summary>
        public Guid UserId { get; set; }

    }
}
