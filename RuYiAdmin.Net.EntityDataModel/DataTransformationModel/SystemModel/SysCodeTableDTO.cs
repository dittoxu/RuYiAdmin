﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel;
using System.Collections.Generic;

namespace RuYiAdmin.Net.EntityDataModel.DataTransformationModel.SystemModel
{
    /// <summary>
    /// 数据字典DTO
    /// </summary>
    public class SysCodeTableDTO : SysCodeTable
    {
        /// <summary>
        /// 子集
        /// </summary>
        public List<SysCodeTableDTO> Children { get; set; }
    }
}
