﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using RuYiAdmin.Net.EntityDataModel.BaseEntityModel;
using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.EntityModel.SystemModel
{
    /// <summary>
    /// 系统附件模型
    /// </summary>
    [SugarTable("sys_attachment")]

    public class SysAttachment : RuYiAdminBaseEntity
    {
        /// <summary>
        /// 附件名称
        /// </summary>
        [Required, MaxLength(1024)]
        [SugarColumn(ColumnName = "FILE_NAME")]
        public string FileName { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "FILE_SIZE")]
        public double FileSize { get; set; }

        /// <summary>
        /// 存储路径
        /// </summary>
        [Required, MaxLength(512)]
        [SugarColumn(ColumnName = "FILE_PATH")]
        public string FilePath { get; set; }

        /// <summary>
        /// 业务编号
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "BUSINESS_ID")]
        public Guid BusinessId { get; set; }
    }
}
