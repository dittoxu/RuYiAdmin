﻿//-----------------------------------------------------------------------
// <Copyright>
// * Copyright (C) 2022 RuYiAdmin All Rights Reserved
// </Copyright>
//-----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RuYiAdmin.Net.CommonInfrastructure.Attributes.Framework.ExcelAttribute;
using RuYiAdmin.Net.CommonInfrastructure.Enums.Business;
using RuYiAdmin.Net.EntityDataModel.SessionScope;
using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace RuYiAdmin.Net.EntityDataModel.BaseEntityModel
{
    /// <summary>
    /// 实体基类模型
    /// </summary>
    public class RuYiAdminBaseEntity
    {
        #region 通用属性

        /// <summary>
        /// 编号
        /// </summary>
        [Required]
        [SugarColumn(IsPrimaryKey = true, ColumnName = "ID")]
        public Guid Id { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [MaxLength(512)]
        [SugarColumn(ColumnName = "REMARK")]
        [ExcelExport("备注")]
        [ExcelImport("备注")]
        public string Remark { get; set; }

        /// <summary>
        /// 逻辑标志位
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "ISDEL")]
        public int IsDel { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "CREATOR")]
        public Guid Creator { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "CREATE_TIME")]
        [ExcelExport("创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 编辑人员
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "MODIFIER")]
        public Guid Modifier { get; set; }

        /// <summary>
        /// 编辑时间
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "MODIFY_TIME")]
        public DateTime ModifyTime { get; set; }


        /// <summary>
        /// 版本编号
        /// </summary>
        [Required]
        [SugarColumn(ColumnName = "VERSION_ID")]
        public Guid VersionId { get; set; }

        #endregion

        #region 通用方法 

        /// <summary>
        /// 转化为json
        /// </summary>
        /// <returns>json字符串</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// 创建赋值
        /// </summary>
        /// <param name="context">HttpContext</param>
        public void Create(IHttpContextAccessor context)
        {
            Id = Guid.NewGuid();

            IsDel = (int)DeletionType.Undeleted;

            var user = RuYiSessionContext.GetCurrentUserInfo(context);

            Creator = user.Id;
            CreateTime = DateTime.Now;

            Modifier = user.Id;
            ModifyTime = DateTime.Now;

            VersionId = Guid.NewGuid();
        }

        /// <summary>
        /// 创建赋值
        /// </summary>
        /// <param name="httpContext">HttpContext</param>
        public void Create(HttpContext httpContext)
        {
            Id = Guid.NewGuid();

            IsDel = (int)DeletionType.Undeleted;

            var user = RuYiSessionContext.GetCurrentUserInfo(httpContext);

            Creator = user.Id;
            CreateTime = DateTime.Now;

            Modifier = user.Id;
            ModifyTime = DateTime.Now;

            VersionId = Guid.NewGuid();
        }

        /// <summary>
        /// 编辑赋值
        /// </summary>
        /// <param name="context">HttpContext</param>
        public void Modify(IHttpContextAccessor context)
        {
            var user = RuYiSessionContext.GetCurrentUserInfo(context);

            Modifier = user.Id;
            ModifyTime = DateTime.Now;

            VersionId = Guid.NewGuid();
        }

        /// <summary>
        /// 编辑赋值
        /// </summary>
        /// <param name="httpContext">HttpContext</param>
        public void Modify(HttpContext httpContext)
        {
            var user = RuYiSessionContext.GetCurrentUserInfo(httpContext);

            Modifier = user.Id;
            ModifyTime = DateTime.Now;

            VersionId = Guid.NewGuid();
        }

        /// <summary>
        /// 逻辑删除赋值
        /// </summary>
        /// <param name="context">HttpContext</param>
        public void Delete(IHttpContextAccessor context)
        {
            IsDel = (int)DeletionType.Deleted;

            var user = RuYiSessionContext.GetCurrentUserInfo(context);

            Modifier = user.Id;
            ModifyTime = DateTime.Now;

            VersionId = Guid.NewGuid();
        }

        /// <summary>
        /// 逻辑删除赋值
        /// </summary>
        /// <param name="httpContext">HttpContext</param>
        public void Delete(HttpContext httpContext)
        {
            IsDel = (int)DeletionType.Deleted;

            var user = RuYiSessionContext.GetCurrentUserInfo(httpContext);

            Modifier = user.Id;
            ModifyTime = DateTime.Now;

            VersionId = Guid.NewGuid();
        }

        #endregion
    }
}
